function x = randPointGaussian(set, p)
% randPointGaussian - generates a random vector according to Gaussian 
% distribution within a given set. The covariance matrix is chosen be
% enclosing the set with an ellipsoid, which constitutes the m-sigma
% confidence set of the Gaussian distribution. To ensure that the selected
% point is within the set, a set enclosure check is performed. If the point 
% is not enclosed, a new vector is generated. 
%
% Syntax:  
%    x = randPointGaussian(dim, mSigma)
%
% Inputs:
%    set - continuous set object
%    p - probability that a value is within the set
%
% Outputs:
%    x - random vector
%
% Reference:
%    [1] Siotani, Minoru (1964). "Tolerance regions for a multivariate 
%        normal population". Annals of the Institute of Statistical 
%        Mathematics. 16 (1): 135–153. 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff
% Written:      19-November-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% enclose set by ellipsoid
E = ellipsoid(set);

% obtain center
c = center(E);

% quantile function for probability p of the chi-squared distribution
quantileFctValue = chi2inv(p,dim(E));

% obtain covariance matrix
Sigma = E.Q/quantileFctValue;

% create sample of normal distribution
x = mvnrnd(c,Sigma,1)';

% correct value if not inclosed
while ~(in(set,x))
    x = mvnrnd(c,Sigma,1)';
end

% % for debugging
% E_sigma = ellipsoid(Sigma,c);
% plot(E_sigma)


%------------- END OF CODE --------------