function Hf=hessianTensor_nonlinearSys(x,u)



 Hf{1} = sparse(3,3);

Hf{1}(1,1) = 3*x(2);
Hf{1}(2,1) = 3*x(1);
Hf{1}(1,2) = 3*x(1);


 Hf{2} = sparse(3,3);

Hf{2}(1,1) = -3*x(2);
Hf{2}(2,1) = -3*x(1);
Hf{2}(1,2) = -3*x(1);
