# Follow these steps to run the adhs21 example, the details of which can be found in the paper:

1. Adjust the Makefile (see \<mascot-sds-root\>/README.md for more detailed instructions on this step).

2. Compile the program by executing the following two commands one by one from the command line:

 		make clean_all
		make

3. Generate controllers by running the binary called "vehicle" by executing the command `./vehicle <arguments>` from the command line, where the arguments are:

	* Length of the hyper-rectangular abstract states along the 1st state dimension.
	* Length of the hyper-rectangular abstract states along the 2nd state dimension.
	* Length of the hyper-rectangular abstract states along the 3rd state dimension.
	* Name of the log file, in quotes, where the synthesis analytics is stored (optional, default is "vehicle.log").
	* Verbosity in a scale 0-2 where 0 represents least verbosity (optional, default is 0).
	* Read the abstract transition system (instead of computing afresh) from file when one is already available: 1 for true and 0 for false (optional, default is 0).
	* Warm-start the under-approximation (of the winning region) computation from the outcome of the over-approximation: 1 for true and 0 for false (optional, default is 0).
    * Address of the folder where the controllers and the other synthesis data are to be stored (optional, default is the subdirectory "data"). 

For example, to run the program with abstract state size (0.1 x 0.1 x 0.1), logfile "vehicle.log", verbosity 2, fresh computation of abstract transition system, with warm-starting enabled, and the controllers being stored in the subdirectory "data_case_1", execute the following instruction from the command line:

	./vehicle 0.1 0.1 0.1 "vehicle.log" 2 0 1 "data_case_1"

Note: the order needs to be maintained while providing the arguments.

4. Various information such as the computation time and the approximation error (represented as the volume of the gap between the over- and the under-approximation) can be read off from the last entry of the logfile (default is "vehicle.log").

5. The controlled trajectory can be simulated by running "simulate.m" from Matlab. The program "simulate.m" implements a Matlab function called "simulate", that takes the following optional name-value pairs as arguments:

	* 'Location'- location of the stored controllers, default is './data'.
	* 'Horizon'- simulation horizon in number of discrete time steps, default is 500.
	* 'MaxDoorOpenCount'- maximum number of times the door can open, default is 500.
	* 'ProbabilityDoorOpening'- the probability with which the door opens when it is closed, default is 0.05.

For example, to run the simulation for 1000 time steps, with MaxDoorOpenCount=50, and ProbabilityDoorOpening=0.1, execute the following from the Matlab command window:

	simulate('Horizon', 1000, 'MaxDoorOpenCount', 50, 'ProbabilityDoorOpening', 0.1)

Note: the name-value argument pairs can be provided in any order.
