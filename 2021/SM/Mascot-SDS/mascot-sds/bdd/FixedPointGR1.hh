/*
 * FixedPointGR1.hh
 *
 *  created on: 09.10.2015
 *      author: rungger
 *      modified by: kaushik mallik
 */

#ifndef FIXEDPOINTGR1_HH_
#define FIXEDPOINTGR1_HH_

#include <iostream>
#include <stdexcept>

#include "cuddObj.hh"
#include "SymbolicModel.hh"

#include "Helper.hh"
#ifdef _OPENMP // Check if compiler flag -fopenmp was active
    #define multi_threading_on true
    #include <omp.h>
    #define OMP_FOR_MIDDLE(Y,YY,C) _Pragma("omp parallel for shared(Y,YY,C)")
    #define OMP_FOR_INNER(X,XX) _Pragma("omp parallel for shared(X,XX)")
    #define OMP_CRITICAL_SEC _Pragma("omp critical(access_master_manager)")
#else // Single threaded version
    #define multi_threading_on false
    #define omp_set_nested(...)
    #define omp_set_num_threads(...)
    #define OMP_FOR_MIDDLE(...)
    #define OMP_FOR_INNER(...)
    #define OMP_CRITICAL_SEC
#endif

namespace scots {
/*
 * class: FixedPointGR1
 *
 *
 * provides the fixed point computation for the Buchi specification with almost sure and worst-case semantics
 *
 */
class FixedPointGR1 {
protected:
    /* var: N_
     * number of parallel managers; minimum value=1
     * the mager of index 0 is called the "Master Manager" */
    size_t N_;
    /* var: ddmgr_ */
    std::vector<Cudd*> ddmgr_;
    /* var: symbolicModel_
     * stores the transition relation */
    std::vector<SymbolicModel*> symbolicModel_;
    /* var: permute
     * stores the permutation array used to swap pre with post variables */
    std::vector<int*> permute_;
    
    /* helper BDDs for maybe transitions */
    /* maybe transition relation */
    std::vector<BDD> R_;
    /* maybe transition relation with cubePost_ abstracted */
    std::vector<BDD> RR_;
    
    /* helper BDDs for sure transitions */
    /* sure transition relation */
    std::vector<BDD> S_;
    /* sure transition relation with cubePost_ abstracted */
    std::vector<BDD> SS_;
    
    /* cubes with input and post variables; used in the existential abstraction  */
    std::vector<BDD> cubePost_;
    std::vector<BDD> cubeInput_;
    
public:
    
    /* function: FixedPointGR1
     *
     * initialize the FixedPointGR1 object with a <SymbolicModel> containing the
     * transition relation
     */
    FixedPointGR1(SymbolicModel *symbolicModel) {
        N_=1; /* default number of managers */
        symbolicModel_.push_back(symbolicModel);
        ddmgr_.push_back(symbolicModel_[0]->ddmgr_);
        /* the permutation array */
        size_t n=ddmgr_[0]->ReadSize();
        int* p = new int[n];
        permute_.push_back(p);
        for(size_t i=0; i<n; i++)
            permute_[0][i]=i;
        for(size_t i=0; i<symbolicModel_[0]->nssVars_; i++)
            permute_[0][symbolicModel_[0]->preVars_[i]]=symbolicModel_[0]->postVars_[i];
        /* create a cube with the input Vars */
        BDD* vars = new BDD[symbolicModel_[0]->nisVars_];
        for (size_t i=0; i<symbolicModel_[0]->nisVars_; i++)
            vars[i]=ddmgr_[0]->bddVar(symbolicModel_[0]->inpVars_[i]);
        cubeInput_.push_back(ddmgr_[0]->bddComputeCube(vars,NULL,symbolicModel_[0]->nisVars_));
        delete[] vars;
        /* create a cube with the post Vars */
        vars = new BDD[symbolicModel_[0]->nssVars_];
        for (size_t i=0; i<symbolicModel_[0]->nssVars_; i++)
            vars[i]=ddmgr_[0]->bddVar(symbolicModel_[0]->postVars_[i]);
        cubePost_.push_back(ddmgr_[0]->bddComputeCube(vars,NULL,symbolicModel_[0]->nssVars_));
        delete[] vars;
        
        /* copy the transition relation */
        R_.push_back(symbolicModel_[0]->maybeTransition_);
        RR_.push_back(R_[0].ExistAbstract(cubePost_[0]));
        S_.push_back(symbolicModel_[0]->sureTransition_);
        SS_.push_back(S_[0].ExistAbstract(cubePost_[0]));
    }
    ~FixedPointGR1() {
        for (size_t i=0; i<N_; i++) {
            delete[] permute_[i];
        }
    }
    /* function: initParallelManagers
     *
     *  initializes a group of managers which will work parallelly during synthesis */
    void initParallelManagers(const int numManagers) {
        /* the number of parallel managers must be greater than 1 */
        if (numManagers<=1) {
            std::ostringstream os;
            os << "The number of parallel managers should be greater than or equal to 1.";
            throw std::invalid_argument(os.str().c_str());
        }
        N_=numManagers+1;
        for (size_t i=1; i<N_; i++) {
            /* initialize new manager */
            Cudd* mgr = new Cudd;
            ddmgr_.push_back(mgr);
            /* initialize the symbolic model for the newly created manager */
            SymbolicModel* tr=new SymbolicModel(*mgr,*symbolicModel_[0]);
            symbolicModel_.push_back(tr);
            /* the bdd variable indices remain the smae, so does the permute arrays */
            size_t n=ddmgr_[0]->ReadSize();
            int* p=new int[n];
            for (size_t j=0; j<n; j++) {
                p[j]=permute_[0][j];
            }
            permute_[i]=p;
            /* transfer the transition bdd-s to the newly created manager */
            R_.push_back(R_[0].Transfer(*mgr));
            RR_.push_back(RR_[0].Transfer(*mgr));
            S_.push_back(S_[0].Transfer(*mgr));
            SS_.push_back(SS_[0].Transfer(*mgr));
            cubePost_.push_back(cubePost_[0].Transfer(*mgr));
            cubeInput_.push_back(cubeInput_[0].Transfer(*mgr));
        }
    }
    /* function: clearParallelManagers
     *
     *  clears the group of managers created during the parallel computation */
    void clearParallelManagers() {
        ddmgr_.resize(1);
        symbolicModel_.resize(1);
        for (size_t i=1; i<N_; i++) {
            delete[] permute_[i];
        }
        permute_.resize(1);
        R_.resize(1);
        RR_.resize(1);
        S_.resize(1);
        SS_.resize(1);
        cubePost_.resize(1);
        cubeInput_.resize(1);
        N_=1;
    }
    /* function: getCubeInput
     *
     * returns the cubeInput */
    inline BDD getCubeInput(const int index=0) {
        return cubeInput_[index];
    }
    
    /* function: cpre
     *
     * computes the controllable predecessor
     *
     * cpre(Zi) = { (x,u) | exists x': (x,u,x') in transitionRelation
     *                    and forall x': (x,u,x') in transitionRelation  => x' in Zi }
     *
     */
    BDD cpre(const char* str, const BDD& Zi, const size_t onIndex)  {
        BDD T; /* the actual transition system */
        BDD TT;
        BDD Z=Zi;
        if(!strcmp(str,"maybe")) {
            T = R_[onIndex];
            TT = RR_[onIndex];
        } else if(!strcmp(str,"sure")) {
            T = S_[onIndex];
            TT = SS_[onIndex];
        } else {
            std::cout << "Unknown type of transition " << str << std::endl;
        }
        /* project onto state alphabet */
        Z=Z.ExistAbstract(cubePost_[onIndex]*cubeInput_[onIndex]);
        /* swap variables */
        Z=Z.Permute(permute_[onIndex]);
        /* find the (state, inputs) pairs with a post outside the safe set */
        BDD nZ = !Z;
        BDD F = T.AndAbstract(nZ,cubePost_[onIndex]);
        /* the remaining (state, input) pairs make up the pre */
        BDD nF = !F;
        BDD preZ= TT.AndAbstract(nF,cubePost_[onIndex]);
        return preZ;
    }
    
    /* function: pre
     *
     * computes the cooperative predecessor
     *
     * pre(Zi) = { (x,u) | exists x': (x,u,x') in transitionRelation
     *                    and x' in Zi }
     *
     */
    BDD pre(const char* str, const BDD Zi, const size_t onIndex)  {
        BDD T; /* the actual transition system */
        BDD Z=Zi;
        if(!strcmp(str,"maybe")) {
            T = R_[onIndex];
        } else if(!strcmp(str,"sure")) {
            T = S_[onIndex];
        } else if(!strcmp(str,"maybe without surity")) {
            T = R_[onIndex] & (!S_[onIndex]);
        } else {
            std::cout << "Unknown type of transition " << str << std::endl;
        }
        /* project onto state alphabet */
        Z=Z.ExistAbstract(cubePost_[onIndex]*cubeInput_[onIndex]);
        /* swap variables */
        Z=Z.Permute(permute_[onIndex]);
        /* find the (state, inputs) pairs with a post inside the safe set */
        BDD preZ = T.AndAbstract(Z,cubePost_[onIndex]);
        return preZ;
    }
    
    /* function: apre
     *
     * computes the almost sure predecessor
     *
     * apre(Y,Z) = { (x,u) | forall x': (x,u,x') in maybeTransition_ => x' in Y
     *                    and exists x' in Z: (x,u,x') in sureTransition_ }
     *
     */
    BDD apre(const BDD Y, const BDD Z, const size_t onIndex)  {
        BDD preZ = cpre("maybe",Y,onIndex) * pre("sure",Z,onIndex);
        return preZ;
    }
    
    /* function: upre
     *
     * computes the uncertain predecessor
     *
     * upre(Y,Z) = { (x,u) | forall x': (x,u,x') in sureTransition_ => x' in Y
     *                    and exists x' in Z: (x,u,x') in maybeTransition_ }
     *
     */
    BDD upre(const BDD Yi, const BDD Z, const size_t onIndex)  {
        BDD Y=Yi;
        /* not same as "cpre("sure",Y) * pre("maybe",Z)":
         it is not required that the sure transition is non-emtpy */
        BDD preZ = pre("maybe",Z,onIndex);
        
        BDD T = S_[onIndex];
        /* TT represents the full product of state space X input space BDD */
        BDD TT = (symbolicModel_[onIndex]->stateSpace_->getSymbolicSet())*(symbolicModel_[onIndex]->inputSpace_->getSymbolicSet());
        
        /* project onto state alphabet */
        Y=Y.ExistAbstract(cubePost_[onIndex]*cubeInput_[onIndex]);
        /* swap variables */
        Y=Y.Permute(permute_[onIndex]);
        /* find the (state, inputs) pairs with a sure post outside Y */
        BDD nY = !Y;
        BDD F = T.AndAbstract(nY,cubePost_[onIndex]);
        /* the remaining (state, input) pairs---including the ones with no outgoing transitions---make up the pre w.r.t. Y */
        BDD nF = !F;
        BDD cpreY= TT.AndAbstract(nF,cubePost_[onIndex]);
        BDD upreYZ = preZ * cpreY;
        return upreYZ;
    }
    /* function: GR1 (with static obstacles)
     *
     * computes the over or under approximaton of almost sure/ worst-case GR(1) fixed point for stochastic systems with m assumptions and n guarantees:
     *
     * The initial_seed could be e.g., the over-approximation of the winning domain.
     *
     *  Input:
     *  
     */
    std::vector<BDD> GR1(const char* str,
                         const std::vector<BDD> A,
                         const std::vector<BDD> G,
                         const BDD initial_seed,
                         const BDD Avoid,
                         const int verbose=0) {
        /* save the original transition relations */
        std::vector<BDD> RR=RR_;
        std::vector<BDD> SS=SS_;
        /* remove avoid (state/input) pairs from transition relation */
        for (size_t i=0; i<N_; i++) {
            RR_[i] &= !Avoid;
            SS_[i] &= !Avoid;
        }
        /* solve GR1_A2_G2 on this modified transition system */
        std::vector<BDD> C = GR1(str,A,G,initial_seed,verbose);
        /* restore transitions */
        RR_ = RR;
        SS_ = SS;
        /* return controller */
        return C;
    }
    
    /* function: GR1
     *
     * computes the over or under approximaton of almost sure/ worst-case GR(1) fixed point for stochastic systems with m assumptions and n guarantees:
     *
     * The initial_seed could be e.g., the over-approximation of the winning domain.
     */
    std::vector<BDD> GR1(const char* str, const std::vector<BDD> A, const std::vector<BDD> G, const BDD initial_seed, const int verbose=0) {
        if (multi_threading_on) {
            initParallelManagers(G.size()*A.size());
        }
        if (verbose) {
            std::cout << "Number of assumptions: " << A.size() << std::endl;
            std::cout << "Number of guarantees: " << G.size() << std::endl;
            for (size_t i=0; i<A.size(); i++) {
                std::cout << "\t Number of minterms in A" << "i: " << A[i].CountMinterm(symbolicModel_[0]->nssVars_) << std::endl;
            }
            std::cout << std::endl;
            for (size_t i=0; i<G.size(); i++) {
                std::cout << "\t Number of minterms in G" << "i: " << G[i].CountMinterm(symbolicModel_[0]->nssVars_) << std::endl;
            }
        }
        /* check if the guarantees are subsets of the state space */
        for (size_t i=0; i<G.size(); i++) {
            std::vector<unsigned int> sup = G[i].SupportIndices();
            for(size_t i=0; i<sup.size();i++) {
                int marker=0;
                for(size_t j=0; j<symbolicModel_[0]->nssVars_; j++) {
                    if (sup[i]==symbolicModel_[0]->preVars_[j])
                        marker=1;
                }
                if(!marker) {
                    std::ostringstream os;
                    os << "Error: GR1: the guarantee set G" << i << " depends on variables outside of the state space.";
                    throw std::invalid_argument(os.str().c_str());
                }
            }
        }
        /* for displaying the iteration count */
        if(verbose)
            std::cout << "Iterations: " << std::endl;
        
        /* initialize the sets for the nested fixed point */
        BDD Z = ddmgr_[0]->bddZero();
        BDD ZZ = initial_seed;
        
        if(verbose)
            std::cout << "Number of total minterms of state-input pairs in the transition relations: " << symbolicModel_[0]->it.CountMinterm(symbolicModel_[0]->nssVars_+symbolicModel_[0]->nisVars_) << std::endl;
        /* the controllers (initially empty):
          *     - C[i] is responsible for the sub-specification G(!A[0]) | ... | G(!A[m]) | (F(G[i] & X(Y[(i+1)%n]))), where Y[(i+1)%n] is C[(i+1)%n]'s domain */
        std::vector<BDD> C;
        for (size_t i=0; i<G.size(); i++) {
            BDD cont=ddmgr_[0]->bddZero();
            C.push_back(cont);
        }
        if (verbose) {
            if(!strcmp(str,"over")) {
                std::cout << "Starting computation of the over-approximation of the almost sure winning region." << std::endl;
            } else if(!strcmp(str,"under")) {
                std::cout << "Starting computation of the under-approximation of the almost sure winning region." << std::endl;
            } else if(!strcmp(str,"wc")) {
                std::cout << "Starting computation of the worst case winning region." << std::endl;
            }
        }
        /* outermost nu loop */
        for (int i=1; Z.ExistAbstract(cubeInput_[0])!=ZZ.ExistAbstract(cubeInput_[0]); i++) {
            Z=ZZ;
            if(verbose)
                std::cout << "\t Iteration " << i << ": Z = " << Z.ExistAbstract(cubeInput_[0]).CountMinterm(symbolicModel_[0]->nssVars_) << " states" << std::endl;
            /* initialize the variables to be used in the middle mu loops */
            std::vector<BDD> Y, YY;
            for (size_t ig=0; ig<G.size(); ig++) {
                BDD y=ddmgr_[0]->bddOne();
                BDD yy=ddmgr_[0]->bddZero();
                Y.push_back(y);
                YY.push_back(yy);
            }
            /* reset the controllers: the controllers correspond to the inputs obtained in the last iteration of the outer nu loop only */
            for (size_t i=0; i<G.size(); i++) {
                C[i]=ddmgr_[0]->bddZero();
            }
            omp_set_nested(2); /* for the parallelization of the 2 nested innermost loops of the GR(1) fixpoint */
            omp_set_num_threads(G.size()); /* spawn as many parallel threads as the number of guarantees */
            OMP_FOR_MIDDLE(Y,YY,C) /* starting parallel for loop over the middle mu fixpoints */
            /* middle Y mu loops */
            for (size_t ig=0; ig<G.size(); ig++) {
                size_t onIndexG=findManagerIndex(ig,0,A.size());
                BDD Yl = transferManager(Y[ig],ddmgr_[onIndexG]);
                BDD YYl = transferManager(YY[ig],ddmgr_[onIndexG]);
                for (int j=1; Yl.ExistAbstract(cubeInput_[onIndexG])!=YYl.ExistAbstract(cubeInput_[onIndexG]); j++) {
                    Yl=YYl;
                    /* initialize the variables to be used in the innermost nu loops */
                    std::vector<BDD> X, XX;
                    for (size_t ia=0; ia<A.size(); ia++) {
                        BDD x=ddmgr_[0]->bddZero();
                        BDD xx=initial_seed;
                        X.push_back(x);
                        XX.push_back(xx);
                    }
                    omp_set_num_threads(A.size()); /* spawn as many parallel threads as the number of assumptions */
                    OMP_FOR_INNER(X,XX) /* starting parallel for loop over the innermost nu fixpoints */
                    /* innermost X nu loops */
                    for (size_t ia=0; ia<A.size(); ia++) {
                        size_t onIndexAG=findManagerIndex(ig,ia,A.size());
                        /* First transfer all the sets to the respective local managers */
                        BDD Gl = transferManager(G[ig],ddmgr_[onIndexAG]);
                        BDD Al = transferManager(A[ia],ddmgr_[onIndexAG]);
                        BDD Zl = transferManager(Z,ddmgr_[onIndexAG]);
                        BDD Yll = transferManager(Yl,ddmgr_[onIndexAG]);
                        BDD Xl = transferManager(X[ia],ddmgr_[onIndexAG]);
                        BDD XXl = transferManager(XX[ia],ddmgr_[onIndexAG]);
                        for (int k=1; Xl.ExistAbstract(cubeInput_[onIndexAG])!=XXl.ExistAbstract(cubeInput_[onIndexAG]); k++) {
                            Xl=XXl;
                            if(verbose) {
                                printf("\t\t Iteration  (%d, %d): Y%lu = %g states, X%lu = %g states \n", j, k, ig, Yll.ExistAbstract(cubeInput_[onIndexAG]).CountMinterm(symbolicModel_[onIndexAG]->nssVars_), ia, Xl.ExistAbstract(cubeInput_[onIndexAG]).CountMinterm(symbolicModel_[onIndexAG]->nssVars_));
                            }
                            /* The update */
                            /* now the local update */
                            if(!strcmp(str,"over")) {
                                XXl= (Gl & (cpre("sure",Zl,onIndexAG) | pre("maybe without surity",Zl,onIndexAG))) | upre(Zl,Yll,onIndexAG) | ((!Al) & (cpre("sure",Xl,onIndexAG) | pre("maybe without surity",Xl,onIndexAG)));
                            } else if(!strcmp(str,"under")) {
                                XXl= (Gl & cpre("maybe",Zl,onIndexAG)) | (apre(Zl,Yll,onIndexAG) | cpre("maybe", Yll,onIndexAG)) | ((!Al) & cpre("maybe",Xl,onIndexAG));
                            } else if(!strcmp(str,"wc")) {
                                XXl= (Gl & cpre("maybe",Zl,onIndexAG)) | cpre("maybe", Yll,onIndexAG) | ((!Al) & cpre("maybe",Xl,onIndexAG));
                            }
                        }
                        OMP_CRITICAL_SEC /* every transfer to the master manager has to be exclusive */
                        {
                            /* transfer back to the master manager */
                            XX[ia]=transferManager(XXl,ddmgr_[0]);
                        }
                    }
                    /* update the middle mu variable */
                    /* to protect the "ExistAbstract", we need to go via a different manager */
                    YYl=ddmgr_[onIndexG]->bddZero();
                    for (size_t ia=0; ia<A.size(); ia++) {
                        YYl |= transferManager(XX[ia],ddmgr_[onIndexG]);
                    }
                    /* controller extraction */
                    BDD Cl = transferManager(C[ig],ddmgr_[onIndexG]);
                    /* new (state, input) pairs */
                    BDD Nl = YYl & (!(Cl.ExistAbstract(cubeInput_[onIndexG])));
                    /* add the new pairs to the local controller copy */
                    Cl = Cl | Nl;
                    OMP_CRITICAL_SEC /* every transfer to the master manager has to be exclusive */
                    {
                    /* transfer back the result to the master manager */
                    C[ig] = transferManager(Cl,ddmgr_[0]);
                    YY[ig] = transferManager(YYl,ddmgr_[0]);
                    }
                }
            }
            ZZ=ddmgr_[0]->bddOne();
            for (size_t ig=0; ig<G.size(); ig++) {
                ZZ &= YY[ig].ExistAbstract(cubeInput_[0]);
            }
        }
        /* clear the parallel managers  */
        if (multi_threading_on) {
            clearParallelManagers();
        }
        /* return the controller */
        return C;
    }
private:
    inline size_t findManagerIndex(const size_t ig, const size_t ia, const size_t Na) {
        if (multi_threading_on) {
            return (ig*Na + ia + 1);
        } else {
            return 0;
        }
    }
    BDD transferManager(const BDD& S, Cudd* M) {
        if (multi_threading_on) {
            return S.Transfer(*M);
        } else {
            return S;
        }
    }
    }; /* close class def */
} /* close namespace */

#endif /* FIXEDPOINT_HH_ */
