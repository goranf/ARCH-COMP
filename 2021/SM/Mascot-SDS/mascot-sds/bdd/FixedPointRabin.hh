/*
 * FixedPointRabin.hh
 *
 *  created on: 09.10.2015
 *      author: rungger
 *      modified by: kaushik mallik
 */

#ifndef FIXEDPOINTRABIN_HH_
#define FIXEDPOINTRABIN_HH_

#include <iostream>
#include <stdexcept>

#include "cuddObj.hh"
#include "SymbolicModel.hh"
#include "RabinAutomaton.hh"

#include "Helper.hh"


namespace scots {
/*
 * class: FixedPointRabin
 *
 *
 * provides the fixed point computation for the Buchi specification with almost sure and worst-case semantics
 *
 */
class FixedPointRabin {
protected:
    /* var: ddmgr_ */
    Cudd *ddmgr_;
    /* var: nssVars_
     * number of BDD variables for state space */
    size_t nssVars_;
    /* var: nisVars_
     * number of BDD variables for input space */
    size_t nisVars_;
    /* var: preVars_
     * array of indices of the state space pre bdd variables  */
    size_t* preVars_;
    /* var: postVars_
     * array of indices of the state space post bdd variables  */
    size_t* postVars_;
    /* var: stateSpaceSymbolicSet_ */
    scots::SymbolicSet* stateSpaceSymbolicSet_;
    /* var: stateSpace_
     * stores the state space BDD */
    BDD stateSpace_;
    /* var: inputSpace_
     * stores the input space BDD */
    BDD inputSpace_;
    /* var: permute
     * stores the permutation array used to swap pre with post variables */
    int* permute_;
    /* helper BDDs for maybe transitions */
    /* maybe transition relation */
    BDD R_;
    /* maybe transition relation with cubePost_ abstracted */
    BDD RR_;
    /* helper BDDs for sure transitions */
    /* sure transition relation */
    BDD S_;
    /* sure transition relation with cubePost_ abstracted */
    BDD SS_;
    /* cubes with input and post variables; used in the existential abstraction  */
    BDD cubePost_;
    BDD cubeInput_;
    /* the symbolic model */
    SymbolicModel* symbolicModel_;
    /* rabin pairs */
    std::vector<mascot::rabin_pair_> RabinPairs_;
public:
    /* constructor: FixedPointRabin
     *
     * initialize the FixedPointRabin object as a product between a <SymbolicModel> containing the transition relation and a <RabinAutomaton> providing the specification
     */
    FixedPointRabin(SymbolicModel* symbolicModel,
               mascot::RabinAutomaton* rabin) {
        /* sanity check: the symbolic model and the rabin automaton must share the same manager */
        if (symbolicModel->ddmgr_->getManager() != rabin->ddmgr_->getManager()) {
            std::ostringstream os;
            os << "Error: The symbolic set and the rabin automaton do no have the same manager.";
            throw std::invalid_argument(os.str().c_str());
        }
        ddmgr_=symbolicModel->ddmgr_;
        nssVars_=symbolicModel->nssVars_+rabin->stateSpace_->getNVars();
        nisVars_=symbolicModel->nisVars_;
        preVars_=new size_t[nssVars_];
        for (size_t i=0; i<symbolicModel->nssVars_; i++) {
            preVars_[i]=symbolicModel->preVars_[i];
        }
        for (int i=0; i<rabin->stateSpace_->getNVars(); i++) {
            preVars_[i+symbolicModel->nssVars_]=rabin->stateSpace_->getIndBddVars()[0][i];
        }
        postVars_=new size_t[nssVars_];
        for (size_t i=0; i<symbolicModel->nssVars_; i++) {
            postVars_[i]=symbolicModel->postVars_[i];
        }
        for (int i=0; i<rabin->stateSpace_->getNVars(); i++) {
            postVars_[i+symbolicModel->nssVars_]=rabin->stateSpacePost_->getIndBddVars()[0][i];
        }
        stateSpaceSymbolicSet_ = new SymbolicSet(*symbolicModel->stateSpace_,*rabin->stateSpace_);
        stateSpaceSymbolicSet_->addGridPoints();
        stateSpace_=stateSpaceSymbolicSet_->getSymbolicSet();
        inputSpace_=symbolicModel->inputSpace_->getSymbolicSet();
        /* the permutation array */
        size_t n=ddmgr_->ReadSize();
        permute_ = new int[n];
        for(size_t i=0; i<n; i++)
            permute_[i]=i;
        for(size_t i=0; i<nssVars_; i++)
            permute_[preVars_[i]]=postVars_[i];
        /* create a cube with the input Vars */
        BDD* vars = new BDD[nisVars_];
        for (size_t i=0; i<nisVars_; i++)
            vars[i]=ddmgr_->bddVar(symbolicModel->inpVars_[i]);
        cubeInput_ = ddmgr_->bddComputeCube(vars,NULL,nisVars_);
        delete[] vars;
        /* create a cube with the post Vars */
        vars = new BDD[nssVars_];
        for (size_t i=0; i<nssVars_; i++)
            vars[i]=ddmgr_->bddVar(postVars_[i]);
        cubePost_ = ddmgr_->bddComputeCube(vars,NULL,nssVars_);
        delete[] vars;
        /* copy the transition relation */
        R_=symbolicModel->maybeTransition_ & rabin->transitions_;
        RR_=R_.ExistAbstract(cubePost_);
        S_=symbolicModel->sureTransition_ & rabin->transitions_;
        SS_=S_.ExistAbstract(cubePost_);
        
        symbolicModel_=symbolicModel;
        
        /* create the rabin pairs */
        for (size_t i=0; i<rabin->RabinPairs_.size(); i++) {
            mascot::rabin_pair_ pair;
            pair.rabin_index_=rabin->RabinPairs_[i].rabin_index_;
            stateSpaceSymbolicSet_->setSymbolicSet(rabin->RabinPairs_[i].G_);
            pair.G_=stateSpaceSymbolicSet_->getSymbolicSet();
            stateSpaceSymbolicSet_->setSymbolicSet(rabin->RabinPairs_[i].nR_);
            pair.nR_=stateSpaceSymbolicSet_->getSymbolicSet();
            RabinPairs_.push_back(pair);
        }
    }
    /* constructor: FixedPointRabin
     *
     * initialize the FixedPointRabin object with a <SymbolicModel> containing the
     * transition relation
     */
    FixedPointRabin(SymbolicModel *symbolicModel) {
        ddmgr_=symbolicModel->ddmgr_;
        nssVars_=symbolicModel->nssVars_;
        nisVars_=symbolicModel->nisVars_;
        preVars_=new size_t[nssVars_];
        for (size_t i=0; i<nssVars_; i++) {
            preVars_[i]=symbolicModel->preVars_[i];
        }
        postVars_=new size_t[nssVars_];
        for (size_t i=0; i<nssVars_; i++) {
            postVars_[i]=symbolicModel->postVars_[i];
        }
        stateSpace_=symbolicModel->stateSpace_->getSymbolicSet();
        stateSpaceSymbolicSet_=new SymbolicSet(*symbolicModel->stateSpace_);
        inputSpace_=symbolicModel->inputSpace_->getSymbolicSet();
        /* the permutation array */
        size_t n=ddmgr_->ReadSize();
        permute_ = new int[n];
        for(size_t i=0; i<n; i++)
            permute_[i]=i;
        for(size_t i=0; i<nssVars_; i++)
            permute_[preVars_[i]]=postVars_[i];
        /* create a cube with the input Vars */
        BDD* vars = new BDD[nisVars_];
        for (size_t i=0; i<nisVars_; i++)
            vars[i]=ddmgr_->bddVar(symbolicModel->inpVars_[i]);
        cubeInput_ = ddmgr_->bddComputeCube(vars,NULL,nisVars_);
        delete[] vars;
        /* create a cube with the post Vars */
        vars = new BDD[nssVars_];
        for (size_t i=0; i<nssVars_; i++)
            vars[i]=ddmgr_->bddVar(postVars_[i]);
        cubePost_ = ddmgr_->bddComputeCube(vars,NULL,nssVars_);
        delete[] vars;
        /* copy the transition relation */
        R_=symbolicModel->maybeTransition_;
        RR_=R_.ExistAbstract(cubePost_);
        S_=symbolicModel->sureTransition_;
        SS_=S_.ExistAbstract(cubePost_);
        
        symbolicModel_=symbolicModel;
    }
    ~FixedPointRabin() {
        delete[] permute_;
        delete[] preVars_;
        delete[] postVars_;
    }
    /* function: getCubeInput
     *
     * returns the cubeInput */
    inline BDD getCubeInput(void) {
        return cubeInput_;
    }
    /* function: Rabin (with static obstacles)
     *
     * computes the over or under approximaton of almost sure/ worst-case Rabin fixed point for stochastic systems
     *
     * Input:
     *  str       - "under" for under-approximation or "over" for over-approximation
     *  accl_on   - true/false setting the accelerated fixpoint on/off
     *  M         - the bound on the iteration count for memorizing the BDDs from the past iterations
     *  rabin     - the rabin automaton
     *  initial_seed  - initial seed for warm starting the nu fixedpoints (for example the under-approximation fixpoint can be warm-started from the result of the over-approximation fixpoint)
     *  Avoid     - the set of static obstacles (which is not part of the specification)
     *  verbose   - the verbosity level (0-2, default=0)
     */
    std::vector<BDD> Rabin(const char* str,
                           const bool accl_on,
                           const size_t M, /* the bound on the iteration count for memorizing the BDDs from the past iterations */
                           const mascot::RabinAutomaton* rabin,
                           const BDD initial_seed,
                           const BDD Avoid,
                           const int verbose=0
                           ) {
        /* save the original transition relations */
        BDD RR=RR_;
        BDD SS=SS_;
        /* remove avoid (state/input) pairs from transition relation */
        RR_ &= !Avoid;
        SS_ &= !Avoid;
        /* solve GR1_A2_G2 on this modified transition system */
        std::vector<BDD> C = Rabin(str,accl_on,M,rabin,initial_seed,verbose);
        /* restore transitions */
        RR_ = RR;
        SS_ = SS;
        /* return controller */
        return C;
    }
    /* function: Rabin
     *
     *  computes the over or under approximation of almost sure/worst-case Rabin fixed point for stochastic systems with k Rabin pairs
     *
     * Input:
     *  str       - "under" for under-approximation or "over" for over-approximation
     *  accl_on   - true/false setting the accelerated fixpoint on/off
     *  M         - the bound on the iteration count for memorizing the BDDs from the past iterations
     *  rabin     - the rabin automaton
     *  initial_seed  - initial seed for warm starting the nu fixedpoints (for example the under-approximation fixpoint can be warm-started from the result of the over-approximation fixpoint)
     *  verbose   - the verbosity level (0-2, default=0)
     */
    std::vector<BDD> Rabin(const char* str,
                           const bool accl_on,
                           const size_t M, /* the bound on the iteration count for memorizing the BDDs from the past iterations */
                           const mascot::RabinAutomaton* rabin,
                           const BDD initial_seed,
                           const int verbose=0) {
        /* copy the rabin pairs */
        std::vector<mascot::rabin_pair_> pairs=RabinPairs_;
        size_t k = pairs.size(); /* number of rabin pairs */
        /* initialize a pair of trivial bdd-s */
        BDD top = ddmgr_->bddOne();
        BDD bot = ddmgr_->bddZero();
        std::vector<std::vector<std::vector<BDD>>>* hist_Y = new std::vector<std::vector<std::vector<BDD>>>;
        std::vector<std::vector<std::vector<BDD>>>* hist_X = new std::vector<std::vector<std::vector<BDD>>>;
        if (accl_on) {
            /* if acceleration is on, then populate hist_Y and hist_X with the respective initial values */
            for (size_t i=0; i<factorial(k); i++) {
                std::vector<std::vector<BDD>> x, y;
                for (size_t j=0; j<pow(M,k+1); j++) {
                    std::vector<BDD> yy(k+1, top);
                    y.push_back(yy);
                    std::vector<BDD> xx(k+1, bot);
                    x.push_back(xx);
                }
                hist_Y->push_back(y);
                hist_X->push_back(x);
            }
        }
        /* create variables for remembering the current indices of the fixpoint variables and the indices of the rabin pairs */
        std::vector<size_t> *indexY = new std::vector<size_t>;
        std::vector<size_t> *indexX = new std::vector<size_t>;
        std::vector<size_t> *indexRP = new std::vector<size_t>;
        /* the controller */
        BDD C;
        /* initialize the sets for the nu fixed point */
        BDD Y = ddmgr_->bddZero();
        BDD YY = initial_seed;
        for (int i=0; Y.ExistAbstract(cubeInput_)!=YY.ExistAbstract(cubeInput_); i++) {
            Y=YY;
            if (accl_on)
                indexY->push_back(i);
            if (verbose==2) {
                std::cout << "Y0, iteration " << i << ", states = " << Y.ExistAbstract(cubeInput_).CountMinterm(nssVars_) << "\n";
            }
            /* reset the controller */
            C = ddmgr_->bddZero();
            /* initialize the sets for the mu fixed point */
            BDD X = ddmgr_->bddOne();
            BDD XX = ddmgr_->bddZero();
            for (int k=0; X.ExistAbstract(cubeInput_)!=XX.ExistAbstract(cubeInput_); k++) {
                X=XX;
                if (accl_on)
                    indexX->push_back(k);
                if (verbose==2) {
                    std::cout << "\t X0, iteration " << k << ", states = " << X.ExistAbstract(cubeInput_).CountMinterm(nssVars_) << "\n";
                }
                BDD term;
                if (!strcmp(str,"under")) {
                    term= apre(Y,X);
                } else if (!strcmp(str,"over")) {
                    term = upre(Y,X);
                }
                /* the state-input pairs added by the outermost loop get the smallest rank */
                BDD N = term & (!(C.ExistAbstract(cubeInput_)));
                C |= N;
                /* recursively solve the rest of the fp */
                XX= RabinRecurse(str, accl_on, M, 1, pairs, initial_seed, ddmgr_->bddOne(), term, C, indexRP, indexY, indexX, hist_Y, hist_X, verbose);
                if (accl_on)
                    indexX->pop_back();
            }
            YY= X;
            if (accl_on)
                indexY->pop_back();
        }
        /* controller vector: size of C_vec= number of rabin states */
        std::vector<BDD> C_vec;
        for (size_t i=static_cast<size_t>(rabin->stateSpace_->getFirstGridPoint()[0]); i<=rabin->stateSpace_->getLastGridPoint()[0]; i++) {
            std::vector<size_t> x={i};
            BDD cube=rabin->stateSpace_->elementToMinterm(x);
            BDD Ci = C & cube;
            C_vec.push_back(Ci);
        }
        return C_vec;
    }
private:
    /* function: cpre
     *
     * computes the controllable predecessor
     *
     * cpre(Zi) = { (x,u) | exists x': (x,u,x') in transitionRelation
     *                    and forall x': (x,u,x') in transitionRelation  => x' in Zi }
     *
     */
    BDD cpre(const char* str, const BDD& Zi)  {
        BDD T; /* the actual transition system */
        BDD TT;
        BDD Z = Zi;
        if(!strcmp(str,"maybe")) {
            T = R_;
            TT = RR_;
        } else if(!strcmp(str,"sure")) {
            T = S_;
            TT = SS_;
        } else {
            std::cout << "Unknown type of transition " << str << std::endl;
        }
        /* project onto state alphabet */
        Z=Z.ExistAbstract(cubePost_*cubeInput_);
        /* swap variables */
        Z=Z.Permute(permute_);
        /* find the (state, inputs) pairs with a post outside the safe set */
        BDD nZ = !Z;
        BDD F = T.AndAbstract(nZ,cubePost_);
        /* the remaining (state, input) pairs make up the pre */
        BDD nF = !F;
        BDD preZ= TT.AndAbstract(nF,cubePost_);
        return preZ;
    }
    
    /* function: pre
     *
     * computes the cooperative predecessor
     *
     * pre(Zi) = { (x,u) | exists x': (x,u,x') in transitionRelation
     *                    and x' in Zi }
     *
     */
    BDD pre(const char* str, const BDD Zi)  {
        BDD T; /* the actual transition system */
        BDD Z=Zi;
        if(!strcmp(str,"maybe")) {
            T = R_;
        } else if(!strcmp(str,"sure")) {
            T = S_;
        } else if(!strcmp(str,"maybe without surity")) {
            T = R_ & (!S_);
        } else {
            std::cout << "Unknown type of transition " << str << std::endl;
        }
        /* project onto state alphabet */
        Z=Z.ExistAbstract(cubePost_*cubeInput_);
        /* swap variables */
        Z=Z.Permute(permute_);
        /* find the (state, inputs) pairs with a post inside the safe set */
        BDD preZ = T.AndAbstract(Z,cubePost_);
        return preZ;
    }
    
    /* function: apre
     *
     * computes the almost sure predecessor
     *
     * apre(Y,Z) = { (x,u) | forall x': (x,u,x') in maybeTransition_ => x' in Y
     *                    and exists x' in Z: (x,u,x') in sureTransition_ } OR cpre("maybe",Z)
     *
     */
    BDD apre(const BDD Y, const BDD Z)  {
        BDD preZ = (cpre("maybe",Y) * pre("sure",Z)) | cpre("maybe",Z);
        return preZ;
    }
    
    /* function: upre
     *
     * computes the uncertain predecessor
     *
     * upre(Y,Z) = { (x,u) | forall x': (x,u,x') in sureTransition_ => x' in Y
     *                    and exists x' in Z: (x,u,x') in maybeTransition_ }
     *
     */
    BDD upre(const BDD Yi, const BDD Z)  {
        BDD Y=Yi;
        /* not same as "cpre("sure",Y) * pre("maybe",Z)":
         it is not required that the sure transition is non-emtpy */
        BDD preZ = pre("maybe",Z);
        
        BDD T = S_;
        /* TT represents the full product of state space X input space BDD */
        BDD TT = stateSpace_*inputSpace_;
        
        /* project onto state alphabet */
        Y=Y.ExistAbstract(cubePost_*cubeInput_);
        /* swap variables */
        Y=Y.Permute(permute_);
        /* find the (state, inputs) pairs with a sure post outside Y */
        BDD nY = !Y;
        BDD F = T.AndAbstract(nY,cubePost_);
        /* the remaining (state, input) pairs---including the ones with no outgoing transitions---make up the pre w.r.t. Y */
        BDD nF = !F;
        BDD cpreY= TT.AndAbstract(nF,cubePost_);
        
        BDD upreYZ = preZ * cpreY;
        return upreYZ;
    }
    /* function: RabinRecurse
     *
     *  recursively solve the Rabin fixpoint (used internally by the function called Rabin) */
    BDD RabinRecurse(const char* str,
                     const bool accl_on,
                     const size_t M, /* the bound on the iteration count for memorizing the BDDs from the past iterations */
                     const int depth,
                     const std::vector<mascot::rabin_pair_> pairs,
                     const BDD initial_seed,
                     BDD seqR,
                     BDD right,
                     BDD &controller,
                     std::vector<size_t> *indexRP,
                     std::vector<size_t> *indexY,
                     std::vector<size_t> *indexX,
                     std::vector<std::vector<std::vector<BDD>>>* hist_Y,
                     std::vector<std::vector<std::vector<BDD>>>* hist_X,
                     const int verbose=0) {
        /* initialize the final solution to be returned in the end */
        BDD U=ddmgr_->bddZero();
        for (size_t i=0; i<pairs.size(); i++) {
            if (verbose==2) {
                printTabs(3*depth-1);
                std::cout << "Remaining pairs " << pairs.size() << "\n\n";
            }
            if (accl_on)
                indexRP->push_back(pairs[i].rabin_index_);
            BDD G = pairs[i].G_;
            BDD nR = pairs[i].nR_;
            std::vector<mascot::rabin_pair_> remPairs=pairs;
            remPairs.erase(remPairs.begin()+i);
            /* initialize a local copy for the controller */
            BDD C = ddmgr_->bddZero();;
            /* initialize the sets for the nu fixed point */
            BDD Y = ddmgr_->bddZero();
            BDD YY;
            if (accl_on && check_threshold(*indexX,M-1)) {
                YY = (*hist_Y)[lexi_order(*indexRP,RabinPairs_.size()-1)][to_dec(M,pad_zeros(*indexX,remPairs.size()+1))][depth];
            } else {
                YY = initial_seed;
            }
            for (int j=0; Y.ExistAbstract(cubeInput_)!=YY.ExistAbstract(cubeInput_); j++) {
                Y=YY;
                if (accl_on)
                    indexY->push_back(j);
                if (verbose==2) {
                    printTabs(3*depth);
                    std::cout << "Y" << depth << ", iteration " << j << ", states = " << Y.ExistAbstract(cubeInput_).CountMinterm(nssVars_) << "\n";
                }
                BDD term1;
                if(!strcmp(str,"under")) {
                    term1 = right | (seqR & nR & G & cpre("maybe",Y));
                } else if (!strcmp(str,"over")) {
                    term1 = right | (seqR & nR & G & upre(Y,Y));
                }
                /* reset the local copy of the controller to the most recently added state-input pairs */
                BDD N = term1 & (!(controller.ExistAbstract(cubeInput_)));
                C=controller | N;
                /* initialize the sets for the mu fixed point */
                BDD X = ddmgr_->bddOne();
                BDD XX;
                if (accl_on && check_threshold(*indexY,M-1)) {
                    XX=(*hist_X)[lexi_order(*indexRP,RabinPairs_.size()-1)][to_dec(M,pad_zeros(*indexY,remPairs.size()))][depth];
                } else {
                    XX = ddmgr_->bddZero();
                }
                for (int k=0; X.ExistAbstract(cubeInput_)!=XX.ExistAbstract(cubeInput_); k++) {
                    X=XX;
                    if (accl_on)
                        indexX->push_back(k);
                    if (verbose==2) {
                        printTabs(3*depth+1);
                        std::cout << "X" << depth << ", iteration " << k << ", states = " << X.ExistAbstract(cubeInput_).CountMinterm(nssVars_) << "\n";
                    }
                    BDD term2;
                    if(!strcmp(str,"under")) {
                        term2= term1 | (seqR & nR & apre(Y,X));
                    } else if (!strcmp(str,"over")) {
                        term2 = term1 | (seqR & nR & upre(Y,X));
                    }
                    /* add the recently added state-input pairs to the controller */
                    N = term2 & (!(C.ExistAbstract(cubeInput_)));
                    C |= N;
                    if (remPairs.size()==0) {
                        XX= term2;
                    } else {
                        XX= RabinRecurse(str, accl_on, M, depth+1, remPairs, initial_seed, seqR & nR, term2, C, indexRP, indexY, indexX, hist_Y, hist_X, verbose);
                    }
                    if (accl_on)
                        indexX->pop_back();
                }
                YY=X;
                if (accl_on) {
                    if (check_threshold(*indexY,M-1)) {
                        (*hist_X)[lexi_order(*indexRP,RabinPairs_.size()-1)][to_dec(M,pad_zeros(*indexY,remPairs.size()))][depth]=X;
                    }
                    indexY->pop_back();
                }
            }
            U |= Y;
            controller = C;
            if (accl_on) {
                if (check_threshold(*indexX,M-1)) {
                    (*hist_Y)[lexi_order(*indexRP,RabinPairs_.size()-1)][to_dec(M,pad_zeros(*indexX,remPairs.size()+1))][depth]=Y;
                }
            }
            if (accl_on)
                indexRP->pop_back();
        }
        return U;
    }
    /* function: printTabs
     *  used to print n number of tabs on the standard I/O during printing the results */
    void printTabs(const int n) {
        for (int i=0; i<n; i++) {
            std::cout << "\t";
        }
    }
    /* function: to_dec
     *  convert a number with base "base" to a decimal number */
    size_t to_dec(const size_t base, const std::vector<size_t> number) {
        size_t N=0;
        for (size_t i=0; i<number.size(); i++) {
            N += number[i]*pow(base,i);
        }
        return N;
    }
    /* function: factorial
     *  compute the factorial */
    size_t factorial(const size_t n) {
        if (n==0) {
            return 1;
        } else {
            return (n*factorial(n-1));
        }
    }
    /* function: lexi_order
     *  determine the position of a given permutation "seq1" in the lexicographically ordered set of all permutations of N numbers */
    size_t lexi_order(const std::vector<size_t> seq1, const size_t N) {
        /* first, make the sequence complete by filling the extra digits with the lexicographically smallest suffix */
        std::vector<size_t> seq2 = seq1;
        if (seq2.size()<N+1) {
            for (size_t i=0; i<=N; i++) {
                bool number_present=false;
                for (size_t j=0; j<seq2.size(); j++) {
                    if (seq2[j]==i) {
                        number_present=true;
                        break;
                    }
                }
                if (!number_present)
                    seq2.push_back(i);
            }
        }
        /* second, compute the Lehmer code of the resulting padded sequence */
        std::vector<size_t> lehmer;
        for (size_t i=0; i<seq2.size(); i++) {
            size_t right_inversion=0;
            for (size_t j=i+1; j<seq2.size(); j++) {
                if (seq2[i]>seq2[j]) {
                    right_inversion++;
                }
            }
            lehmer.push_back(right_inversion);
        }
        /* third, compute the decimal value of the Lehmer code, interpreting it as a factorial base number */
        size_t pos=0;
        for (size_t i=0; i<lehmer.size(); i++) {
            pos+= factorial(i)*lehmer[i];
        }
        return pos;
    }
    /* function: pad_zeros
     *  pad zeros to a vector "vec1" to make its size equal to n */
    inline std::vector<size_t> pad_zeros(const std::vector<size_t> vec1, const size_t n) {
        std::vector<size_t> vec2=vec1;
        for (size_t i=0; i<n; i++) {
            vec2.push_back(0);
        }
        return vec2;
    }
    /* function: check_threshold
     *  returns true if all the elements in the vector called "vec" are below a given threshold */
    inline bool check_threshold(const std::vector<size_t> vec, const size_t th) {
        for (size_t i=0; i<vec.size(); i++) {
            if (vec[i]>th) {
                return false;
            }
        }
        return true;
    }
    }; /* close class def */
} /* close namespace */

#endif /* FIXEDPOINT_HH_ */
