function initErrorCodex

% define as global variables for simpler syntax in config file
global codex;

% init empty struct
codex = struct();
% introduce fields
codex.id = {};
codex.text = {};

% add error messages using aux. func. 'appendErrMsg'
% note: write text such that the first word is the name of the param/option
%       -> example: 'has to be a scalar value'
%                   becomes, e.g.,
%                   'options.timeStep has to be a scalar value.'
%       (the dot will also be added automatically)

% error message in c_* function
appendErrMsg('','see c_* function');

% type checks
appendErrMsg('isscalar','has to be a scalar value');
appendErrMsg('isnumeric','has to be a numeric value');
appendErrMsg('islogical','has to be a logical (true/false) value');
appendErrMsg('ischar','has to be a char array');
appendErrMsg('iscell','has to be a cell array');
% isa(...) checks
appendErrMsg('isafunction_handle','has to be a function handle');
appendErrMsg('isacontSet','has to be an object of class contSet');
appendErrMsg('isazonotope','has to be an object of class zonotope');

% absolute comparisons
appendErrMsg('gtzero','has to be a value greater than 0');
appendErrMsg('gezero','has to be a value greater than or equal to 0');
appendErrMsg('vectorgezero','has to have a value greater than or equal to 0 in all entries');
appendErrMsg('geone','has to be a value greater than or equal to 1');
appendErrMsg('vectorgeone','has to have a value greater than or equal to 1 in all entries');
appendErrMsg('normalized','has to be a value in [0,1]');
appendErrMsg('integer','has to be an integer value');
appendErrMsg('integerorInf','has to be an integer value or Inf');
appendErrMsg('vectororinterval','has to be a vector or an object of class interval');


% not in list of admissible values specified in getMembers
appendErrMsg('memberR0','...');
appendErrMsg('memberU','...');
appendErrMsg('memberlinAlg','...');
appendErrMsg('memberlinAlg4HA','...');
appendErrMsg('memberalg','...');
appendErrMsg('memberalg4param','...');
appendErrMsg('memberreductionTechnique','...');
appendErrMsg('memberreductionTechnique4nlsys','...');
appendErrMsg('memberreductionTechniqueUnderApprox','...');
appendErrMsg('memberlagrangeRem.simplify','...');
appendErrMsg('memberlagrangeRem.method','...');
appendErrMsg('memberlagrangeRem.zooMethods','...');
appendErrMsg('memberlagrangeRem.optMethod','...');
appendErrMsg('memberrestructureTechnique','...');
appendErrMsg('membercontractor','...');
appendErrMsg('memberguardIntersect','...');
appendErrMsg('memberenclose','...');

% comparisons to obj
appendErrMsg('eqsysdim','has to be equal to the system dimension');
appendErrMsg('eqconstr','has to be equal to the number of constraints');
appendErrMsg('eqparam','has to be equal to the number of parameters');

% comparisons to other params/options
appendErrMsg('getStart','has to be greater than params.tStart');
appendErrMsg('intsteps','has to divide the time horizon into an integer number steps');
appendErrMsg('letaylorTerms','has to be less or equal to options.taylorTerms');
appendErrMsg('lecomp','has to be less or equal to the number of components');
appendErrMsg('leloc','has to be less or equal to the number of locations');


% note: global variable 'codex' cleared at the end of validateOptions

end


function appendErrMsg(newid,newtext)

% enable access to codex
global codex;

codex.id{end+1,1} = newid;
% if id contains member, get list of possible values from getMembers.m
if contains(newid,'member')
    codex.text{end+1,1} = newtext; % TODO
else
    codex.text{end+1,1} = newtext;
end

end