function Hf=hessianTensorInt_SUTRACovid_fixed(x,u)



 Hf{1} = interval(sparse(7,7),sparse(7,7));

Hf{1}(3,1) = -1/4;
Hf{1}(4,1) = -1/4;
Hf{1}(1,3) = -1/4;
Hf{1}(1,4) = -1/4;


 Hf{2} = interval(sparse(7,7),sparse(7,7));

Hf{2}(3,2) = -1/4;
Hf{2}(4,2) = -1/4;
Hf{2}(2,3) = -1/4;
Hf{2}(2,4) = -1/4;


 Hf{3} = interval(sparse(7,7),sparse(7,7));

Hf{3}(3,1) = 1/4;
Hf{3}(4,1) = 1/4;
Hf{3}(1,3) = 1/4;
Hf{3}(1,4) = 1/4;


 Hf{4} = interval(sparse(7,7),sparse(7,7));

Hf{4}(3,2) = 1/4;
Hf{4}(4,2) = 1/4;
Hf{4}(2,3) = 1/4;
Hf{4}(2,4) = 1/4;


 Hf{5} = interval(sparse(7,7),sparse(7,7));



 Hf{6} = interval(sparse(7,7),sparse(7,7));

