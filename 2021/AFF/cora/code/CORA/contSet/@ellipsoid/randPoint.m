function p = randPoint(E,varargin)
% randPoint - generates a random point within an ellipsoid
%
% Syntax:  
%    p = randPoint(obj)
%    p = randPoint(obj,N)
%    p = randPoint(obj,N,type)
%
% Inputs:
%    obj - ellipsoid object
%    N - number of random points
%    type - type of the random point ('extreme' or 'normal')
%
% Outputs:
%    p - random point in R^n
%
% Example: 
%    E = ellipsoid([1;0],rand(2,5));
%    p = randPoint(E);
% 
%    plot(E); hold on;
%    scatter(p(1,:),p(2,:),16,'r');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: interval/randPoint

% Author:        Victor Gassmann
% Written:       18-March-2021
% Last update:   ---
% Last revision: ---

%------------- BEGIN CODE --------------
% NOTE: This function does not produce a uniform distribution!
% parse input arguments
if ~isa(E,'ellipsoid')
    error('First argument has to be of type "ellipsoid"!');
end
default = 'normal';
if isempty(varargin)
    N = 1;
    type = default;
elseif length(varargin)==1
    if isa(varargin{1},'double') && isscalar(varargin{1}) && mod(varargin{1},1)==0
        N = varargin{1};
        type = default;
    elseif isa(varargin{1},'char')
        type = varargin{1};
        N = 1;
    else
        error('Wrong type for second input argument!');
    end
elseif length(varargin)==2
    if ~(isa(varargin{1},'double') && isscalar(varargin{1}) && mod(varargin{1},1)==0)...
            || ~isa(varargin{2},'char')
        error('Wrong type of input arguments!');
    end
    N = varargin{1};
    type = varargin{2};
else
    error('Function only supports up to 3 input arguments!');
end

if rank(E)==0
    p = repmat(E.q,1,N);
    return;
end
c = E.q;
E = E + (-c);
r = rank(E);
n = dim(E);
n_rem = n-r;
[T,~,~] = svd(E.Q);
E = T'*E;
% if degenerate, project
E = project(E,1:r);
G = inv(sqrtm(E.Q));
E = G*E;

% generate points uniformely distributed (with N-> infinity) on the unit
% hypersphere
X = randn(dim(E),N);
pt = X./sqrt(sum(X.^2,1));



% generate different types of extreme points
if strcmp(type,'normal')
    S = 2*rand(1,N)-1;
    pt = S.*pt;

elseif ~strcmp(type,'extreme')
    [msg,id] = errWrongInput('type');
    error(id,msg);
end

% stack again, backtransform and shift
p = T*[inv(G)*pt;zeros(n_rem,N)] + c;
%------------- END OF CODE --------------