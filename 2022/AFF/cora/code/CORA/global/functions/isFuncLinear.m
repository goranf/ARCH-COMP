function res = isFuncLinear(f,varargin)
% isFuncLinear - checks which equations of a (vector- or matrix-valued)
%    function handle depend linearly on the input arguments
%    caution: this function is only probabilistically correct!
%
% Syntax:  
%    res = isFuncLinear(f)
%    res = isFuncLinear(f,inputArgs)
%
% Inputs:
%    f - function handle
%    inputArgs - (optional) vector with size of each input argument to f
%
% Outputs:
%    res - logical vector whether i-th function is a linear function
%
% Example: 
%    f = @(x,u) [x(1) - u(2); x(3)^2*x(2) - u(1)];
%    res = isFuncLinear(f,[3 2])
%
% References:
%    ---
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: withinTol, numberOfInputs

% Author:       Mark Wetzlinger
% Written:      05-July-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% check if vector length for each input argument of f is given
if nargin == 1
    % read out length of each input argument
    sizeInputArgs = numberOfInputs(f);
elseif nargin == 2
    sizeInputArgs = varargin{1};
elseif nargin > 2
    throw(CORAerror('CORA:tooManyInputArgs',2));
end

% linearity of a function depends on two criteria:
% 1. forall x,y \in domain:                 f(x+y) = f(x) + f(y)
% 2. for x \in domain and a scalar alpha:   f(alpha*x) = alpha*f(x)

numInputArgs = length(sizeInputArgs);
depth = 4;

% define scalar and vectors to call the function (truncate random numbers
% to avoid floating-point errors in evaluation as much as possible)

% random scalar (non-zero)
alpha = 0;
while alpha == 0
    alpha = round(randn,depth);
end

% pre-allocate cell arrays for vector calling the function
x = cell(numInputArgs,1);
alphax = cell(numInputArgs,1);
y = cell(numInputArgs,1);
xy = cell(numInputArgs,1);

% init counter
i = 0;
while i < numInputArgs
    i = i + 1;

    % random values for i-th input argument
    x{i,1} = round(randn(sizeInputArgs(i),1),depth);
    y{i,1} = round(randn(sizeInputArgs(i),1),depth);
    % ensure that no values add up to 0: otherwise try other random values
    if ~all(x{i} + y{i})
        i = i - 1;
        continue;
    end

    % compute scalar multiplication and addition
    alphax{i,1} = alpha * x{i};
    xy{i,1} = x{i} + y{i};
end

% evaluate function
fx = f(x{:});
fy = f(y{:});
fxy = f(xy{:});
falphax = f(alphax{:});

% check criteria: use relative floating-point accuracy
res = withinTol(fx+fy,fxy,eps) & withinTol(alpha*fx,falphax,eps);

%------------- END OF CODE --------------
