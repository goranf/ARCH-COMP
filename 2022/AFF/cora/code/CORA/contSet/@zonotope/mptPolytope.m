function P = mptPolytope(Z)
% mptPolytope - converts a zonotope object to a mptPolytope object
%
% Syntax:  
%    P = mptPolytope(Z)
%
% Inputs:
%    Z - zonotope object
%
% Outputs:
%    P - mptPolytope object
%
% Example: 
%    Z = zonotope([1;-1],[3 2 -1; 1 -2 2]);
%    P = mptPolytope(Z);
%
%    figure; hold on;
%    plot(P,[1,2],'r');
%    plot(Z,[1,2],'b');
%
% Other m-files required: polytope
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author:       Niklas Kochdumper
% Written:      06-August-2018
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

P = polytope(Z,'mpt');
    
%------------- END OF CODE --------------