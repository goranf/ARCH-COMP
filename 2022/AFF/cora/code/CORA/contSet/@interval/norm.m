function val = norm(I,varargin)
% norm - computes the exact maximum norm value of specified norm
%
% Syntax:  
%    val = norm(I,varargin)
%
% Inputs:
%    I - interval object
%    type - (optional) additional arguments of builtin/norm
%
% Outputs:
%    val - norm value
%
% Example:
%    I = interval([-2;1],[3;2]);
%    norm(I,2)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: mtimes

% Author:       Victor Gassmann
% Written:      31-July-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% default input arguments
[type] = setDefaultValues({{2}},varargin{:});

% check input arguments
if ~isnumeric(type) && ~strcmp(type,'fro')
    throw(CORAerror('CORA:wrongValue','second',"be 1, 2, Inf, or 'fro'"));
end

if isnumeric(type) && type==2
    val = norm(max(abs(I.inf),abs(I.sup)));
else
    val = norm(intervalMatrix(center(I),rad(I)),type);
end

%------------- END OF CODE --------------