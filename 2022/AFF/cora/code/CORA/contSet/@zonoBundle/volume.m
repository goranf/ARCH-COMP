function res = volume(zB)
% volume - Computes the volume of a zonotope bundle
%
% Syntax:  
%    res = volume(zB)
%
% Inputs:
%    zB - zonoBundle object
%
% Outputs:
%    res - volume
%
% Example: 
%    ---
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff
% Written:      02-February-2011 
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

%obtain polytope of zonotope bundle
P = polytope(zB);

%compute volume
res = volume(P);

%------------- END OF CODE --------------
