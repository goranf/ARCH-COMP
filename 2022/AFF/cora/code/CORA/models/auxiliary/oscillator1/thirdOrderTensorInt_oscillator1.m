function [Tf,ind] = thirdOrderTensorInt_oscillator1(x,u)



 Tf{1,1} = interval(sparse(6,6),sparse(6,6));



 Tf{1,2} = interval(sparse(6,6),sparse(6,6));



 Tf{1,3} = interval(sparse(6,6),sparse(6,6));



 Tf{1,4} = interval(sparse(6,6),sparse(6,6));



 Tf{1,5} = interval(sparse(6,6),sparse(6,6));



 Tf{1,6} = interval(sparse(6,6),sparse(6,6));



 Tf{2,1} = interval(sparse(6,6),sparse(6,6));



 Tf{2,2} = interval(sparse(6,6),sparse(6,6));



 Tf{2,3} = interval(sparse(6,6),sparse(6,6));



 Tf{2,4} = interval(sparse(6,6),sparse(6,6));



 Tf{2,5} = interval(sparse(6,6),sparse(6,6));



 Tf{2,6} = interval(sparse(6,6),sparse(6,6));



 Tf{3,1} = interval(sparse(6,6),sparse(6,6));



 Tf{3,2} = interval(sparse(6,6),sparse(6,6));



 Tf{3,3} = interval(sparse(6,6),sparse(6,6));



 Tf{3,4} = interval(sparse(6,6),sparse(6,6));



 Tf{3,5} = interval(sparse(6,6),sparse(6,6));



 Tf{3,6} = interval(sparse(6,6),sparse(6,6));



 Tf{4,1} = interval(sparse(6,6),sparse(6,6));



 Tf{4,2} = interval(sparse(6,6),sparse(6,6));



 Tf{4,3} = interval(sparse(6,6),sparse(6,6));



 Tf{4,4} = interval(sparse(6,6),sparse(6,6));



 Tf{4,5} = interval(sparse(6,6),sparse(6,6));



 Tf{4,6} = interval(sparse(6,6),sparse(6,6));



 Tf{5,1} = interval(sparse(6,6),sparse(6,6));



 Tf{5,2} = interval(sparse(6,6),sparse(6,6));

Tf{5,2}(3,3) = 600*x(3)^2*x(5)^2;
Tf{5,2}(5,3) = 400*x(3)^3*x(5);
Tf{5,2}(3,5) = 400*x(3)^3*x(5);
Tf{5,2}(5,5) = 100*x(3)^4;


 Tf{5,3} = interval(sparse(6,6),sparse(6,6));

Tf{5,3}(3,2) = 600*x(3)^2*x(5)^2;
Tf{5,3}(5,2) = 400*x(3)^3*x(5);
Tf{5,3}(2,3) = 600*x(3)^2*x(5)^2;
Tf{5,3}(3,3) = 120*x(3)*x(5)^2*(10*x(2) - (3*x(3))/2) - 270*x(3)^2*x(5)^2;
Tf{5,3}(5,3) = 120*x(3)^2*x(5)*(10*x(2) - (3*x(3))/2) - 120*x(3)^3*x(5);
Tf{5,3}(2,5) = 400*x(3)^3*x(5);
Tf{5,3}(3,5) = 120*x(3)^2*x(5)*(10*x(2) - (3*x(3))/2) - 120*x(3)^3*x(5);
Tf{5,3}(5,5) = 40*x(3)^3*(10*x(2) - (3*x(3))/2) - 15*x(3)^4;


 Tf{5,4} = interval(sparse(6,6),sparse(6,6));



 Tf{5,5} = interval(sparse(6,6),sparse(6,6));

Tf{5,5}(3,2) = 400*x(3)^3*x(5);
Tf{5,5}(5,2) = 100*x(3)^4;
Tf{5,5}(2,3) = 400*x(3)^3*x(5);
Tf{5,5}(3,3) = 120*x(3)^2*x(5)*(10*x(2) - (3*x(3))/2) - 120*x(3)^3*x(5);
Tf{5,5}(5,3) = 40*x(3)^3*(10*x(2) - (3*x(3))/2) - 15*x(3)^4;
Tf{5,5}(2,5) = 100*x(3)^4;
Tf{5,5}(3,5) = 40*x(3)^3*(10*x(2) - (3*x(3))/2) - 15*x(3)^4;


 Tf{5,6} = interval(sparse(6,6),sparse(6,6));


 ind = cell(5,1);
 ind{1} = [];


 ind{2} = [];


 ind{3} = [];


 ind{4} = [];


 ind{5} = [2;3;5];

