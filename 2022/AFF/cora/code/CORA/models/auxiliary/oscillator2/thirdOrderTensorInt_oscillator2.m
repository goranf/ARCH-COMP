function [Tf,ind] = thirdOrderTensorInt_oscillator2(x,u)



 Tf{1,1} = interval(sparse(11,11),sparse(11,11));



 Tf{1,2} = interval(sparse(11,11),sparse(11,11));



 Tf{1,3} = interval(sparse(11,11),sparse(11,11));



 Tf{1,4} = interval(sparse(11,11),sparse(11,11));



 Tf{1,5} = interval(sparse(11,11),sparse(11,11));



 Tf{1,6} = interval(sparse(11,11),sparse(11,11));



 Tf{1,7} = interval(sparse(11,11),sparse(11,11));



 Tf{1,8} = interval(sparse(11,11),sparse(11,11));



 Tf{1,9} = interval(sparse(11,11),sparse(11,11));



 Tf{1,10} = interval(sparse(11,11),sparse(11,11));



 Tf{1,11} = interval(sparse(11,11),sparse(11,11));



 Tf{2,1} = interval(sparse(11,11),sparse(11,11));



 Tf{2,2} = interval(sparse(11,11),sparse(11,11));



 Tf{2,3} = interval(sparse(11,11),sparse(11,11));



 Tf{2,4} = interval(sparse(11,11),sparse(11,11));



 Tf{2,5} = interval(sparse(11,11),sparse(11,11));



 Tf{2,6} = interval(sparse(11,11),sparse(11,11));



 Tf{2,7} = interval(sparse(11,11),sparse(11,11));



 Tf{2,8} = interval(sparse(11,11),sparse(11,11));



 Tf{2,9} = interval(sparse(11,11),sparse(11,11));



 Tf{2,10} = interval(sparse(11,11),sparse(11,11));



 Tf{2,11} = interval(sparse(11,11),sparse(11,11));



 Tf{3,1} = interval(sparse(11,11),sparse(11,11));



 Tf{3,2} = interval(sparse(11,11),sparse(11,11));



 Tf{3,3} = interval(sparse(11,11),sparse(11,11));



 Tf{3,4} = interval(sparse(11,11),sparse(11,11));



 Tf{3,5} = interval(sparse(11,11),sparse(11,11));



 Tf{3,6} = interval(sparse(11,11),sparse(11,11));



 Tf{3,7} = interval(sparse(11,11),sparse(11,11));



 Tf{3,8} = interval(sparse(11,11),sparse(11,11));



 Tf{3,9} = interval(sparse(11,11),sparse(11,11));



 Tf{3,10} = interval(sparse(11,11),sparse(11,11));



 Tf{3,11} = interval(sparse(11,11),sparse(11,11));



 Tf{4,1} = interval(sparse(11,11),sparse(11,11));



 Tf{4,2} = interval(sparse(11,11),sparse(11,11));



 Tf{4,3} = interval(sparse(11,11),sparse(11,11));



 Tf{4,4} = interval(sparse(11,11),sparse(11,11));



 Tf{4,5} = interval(sparse(11,11),sparse(11,11));



 Tf{4,6} = interval(sparse(11,11),sparse(11,11));



 Tf{4,7} = interval(sparse(11,11),sparse(11,11));



 Tf{4,8} = interval(sparse(11,11),sparse(11,11));



 Tf{4,9} = interval(sparse(11,11),sparse(11,11));



 Tf{4,10} = interval(sparse(11,11),sparse(11,11));



 Tf{4,11} = interval(sparse(11,11),sparse(11,11));



 Tf{5,1} = interval(sparse(11,11),sparse(11,11));



 Tf{5,2} = interval(sparse(11,11),sparse(11,11));

Tf{5,2}(3,3) = 600*x(3)^2*x(5)^2;
Tf{5,2}(5,3) = 400*x(3)^3*x(5);
Tf{5,2}(3,5) = 400*x(3)^3*x(5);
Tf{5,2}(5,5) = 100*x(3)^4;


 Tf{5,3} = interval(sparse(11,11),sparse(11,11));

Tf{5,3}(3,2) = 600*x(3)^2*x(5)^2;
Tf{5,3}(5,2) = 400*x(3)^3*x(5);
Tf{5,3}(2,3) = 600*x(3)^2*x(5)^2;
Tf{5,3}(3,3) = 120*x(3)*x(5)^2*(10*x(2) - (3*x(3))/2) - 270*x(3)^2*x(5)^2;
Tf{5,3}(5,3) = 120*x(3)^2*x(5)*(10*x(2) - (3*x(3))/2) - 120*x(3)^3*x(5);
Tf{5,3}(2,5) = 400*x(3)^3*x(5);
Tf{5,3}(3,5) = 120*x(3)^2*x(5)*(10*x(2) - (3*x(3))/2) - 120*x(3)^3*x(5);
Tf{5,3}(5,5) = 40*x(3)^3*(10*x(2) - (3*x(3))/2) - 15*x(3)^4;


 Tf{5,4} = interval(sparse(11,11),sparse(11,11));



 Tf{5,5} = interval(sparse(11,11),sparse(11,11));

Tf{5,5}(3,2) = 400*x(3)^3*x(5);
Tf{5,5}(5,2) = 100*x(3)^4;
Tf{5,5}(2,3) = 400*x(3)^3*x(5);
Tf{5,5}(3,3) = 120*x(3)^2*x(5)*(10*x(2) - (3*x(3))/2) - 120*x(3)^3*x(5);
Tf{5,5}(5,3) = 40*x(3)^3*(10*x(2) - (3*x(3))/2) - 15*x(3)^4;
Tf{5,5}(2,5) = 100*x(3)^4;
Tf{5,5}(3,5) = 40*x(3)^3*(10*x(2) - (3*x(3))/2) - 15*x(3)^4;


 Tf{5,6} = interval(sparse(11,11),sparse(11,11));



 Tf{5,7} = interval(sparse(11,11),sparse(11,11));



 Tf{5,8} = interval(sparse(11,11),sparse(11,11));



 Tf{5,9} = interval(sparse(11,11),sparse(11,11));



 Tf{5,10} = interval(sparse(11,11),sparse(11,11));



 Tf{5,11} = interval(sparse(11,11),sparse(11,11));



 Tf{6,1} = interval(sparse(11,11),sparse(11,11));



 Tf{6,2} = interval(sparse(11,11),sparse(11,11));



 Tf{6,3} = interval(sparse(11,11),sparse(11,11));



 Tf{6,4} = interval(sparse(11,11),sparse(11,11));



 Tf{6,5} = interval(sparse(11,11),sparse(11,11));



 Tf{6,6} = interval(sparse(11,11),sparse(11,11));



 Tf{6,7} = interval(sparse(11,11),sparse(11,11));



 Tf{6,8} = interval(sparse(11,11),sparse(11,11));



 Tf{6,9} = interval(sparse(11,11),sparse(11,11));



 Tf{6,10} = interval(sparse(11,11),sparse(11,11));



 Tf{6,11} = interval(sparse(11,11),sparse(11,11));



 Tf{7,1} = interval(sparse(11,11),sparse(11,11));



 Tf{7,2} = interval(sparse(11,11),sparse(11,11));



 Tf{7,3} = interval(sparse(11,11),sparse(11,11));



 Tf{7,4} = interval(sparse(11,11),sparse(11,11));



 Tf{7,5} = interval(sparse(11,11),sparse(11,11));



 Tf{7,6} = interval(sparse(11,11),sparse(11,11));



 Tf{7,7} = interval(sparse(11,11),sparse(11,11));



 Tf{7,8} = interval(sparse(11,11),sparse(11,11));



 Tf{7,9} = interval(sparse(11,11),sparse(11,11));



 Tf{7,10} = interval(sparse(11,11),sparse(11,11));



 Tf{7,11} = interval(sparse(11,11),sparse(11,11));



 Tf{8,1} = interval(sparse(11,11),sparse(11,11));



 Tf{8,2} = interval(sparse(11,11),sparse(11,11));



 Tf{8,3} = interval(sparse(11,11),sparse(11,11));



 Tf{8,4} = interval(sparse(11,11),sparse(11,11));



 Tf{8,5} = interval(sparse(11,11),sparse(11,11));



 Tf{8,6} = interval(sparse(11,11),sparse(11,11));



 Tf{8,7} = interval(sparse(11,11),sparse(11,11));



 Tf{8,8} = interval(sparse(11,11),sparse(11,11));



 Tf{8,9} = interval(sparse(11,11),sparse(11,11));



 Tf{8,10} = interval(sparse(11,11),sparse(11,11));



 Tf{8,11} = interval(sparse(11,11),sparse(11,11));



 Tf{9,1} = interval(sparse(11,11),sparse(11,11));



 Tf{9,2} = interval(sparse(11,11),sparse(11,11));



 Tf{9,3} = interval(sparse(11,11),sparse(11,11));



 Tf{9,4} = interval(sparse(11,11),sparse(11,11));



 Tf{9,5} = interval(sparse(11,11),sparse(11,11));



 Tf{9,6} = interval(sparse(11,11),sparse(11,11));



 Tf{9,7} = interval(sparse(11,11),sparse(11,11));



 Tf{9,8} = interval(sparse(11,11),sparse(11,11));



 Tf{9,9} = interval(sparse(11,11),sparse(11,11));



 Tf{9,10} = interval(sparse(11,11),sparse(11,11));



 Tf{9,11} = interval(sparse(11,11),sparse(11,11));



 Tf{10,1} = interval(sparse(11,11),sparse(11,11));



 Tf{10,2} = interval(sparse(11,11),sparse(11,11));



 Tf{10,3} = interval(sparse(11,11),sparse(11,11));



 Tf{10,4} = interval(sparse(11,11),sparse(11,11));



 Tf{10,5} = interval(sparse(11,11),sparse(11,11));



 Tf{10,6} = interval(sparse(11,11),sparse(11,11));



 Tf{10,7} = interval(sparse(11,11),sparse(11,11));

Tf{10,7}(8,8) = 600*x(8)^2*x(10)^2;
Tf{10,7}(10,8) = 400*x(8)^3*x(10);
Tf{10,7}(8,10) = 400*x(8)^3*x(10);
Tf{10,7}(10,10) = 100*x(8)^4;


 Tf{10,8} = interval(sparse(11,11),sparse(11,11));

Tf{10,8}(8,7) = 600*x(8)^2*x(10)^2;
Tf{10,8}(10,7) = 400*x(8)^3*x(10);
Tf{10,8}(7,8) = 600*x(8)^2*x(10)^2;
Tf{10,8}(8,8) = 120*x(8)*x(10)^2*(10*x(7) - (3*x(8))/2) - 270*x(8)^2*x(10)^2;
Tf{10,8}(10,8) = 120*x(8)^2*x(10)*(10*x(7) - (3*x(8))/2) - 120*x(8)^3*x(10);
Tf{10,8}(7,10) = 400*x(8)^3*x(10);
Tf{10,8}(8,10) = 120*x(8)^2*x(10)*(10*x(7) - (3*x(8))/2) - 120*x(8)^3*x(10);
Tf{10,8}(10,10) = 40*x(8)^3*(10*x(7) - (3*x(8))/2) - 15*x(8)^4;


 Tf{10,9} = interval(sparse(11,11),sparse(11,11));



 Tf{10,10} = interval(sparse(11,11),sparse(11,11));

Tf{10,10}(8,7) = 400*x(8)^3*x(10);
Tf{10,10}(10,7) = 100*x(8)^4;
Tf{10,10}(7,8) = 400*x(8)^3*x(10);
Tf{10,10}(8,8) = 120*x(8)^2*x(10)*(10*x(7) - (3*x(8))/2) - 120*x(8)^3*x(10);
Tf{10,10}(10,8) = 40*x(8)^3*(10*x(7) - (3*x(8))/2) - 15*x(8)^4;
Tf{10,10}(7,10) = 100*x(8)^4;
Tf{10,10}(8,10) = 40*x(8)^3*(10*x(7) - (3*x(8))/2) - 15*x(8)^4;


 Tf{10,11} = interval(sparse(11,11),sparse(11,11));


 ind = cell(10,1);
 ind{1} = [];


 ind{2} = [];


 ind{3} = [];


 ind{4} = [];


 ind{5} = [2;3;5];


 ind{6} = [];


 ind{7} = [];


 ind{8} = [];


 ind{9} = [];


 ind{10} = [7;8;10];

