classdef specification
% specification - class that stores specifications
%
% Syntax:  
%    obj = specification(set)
%    obj = specification(list)
%    obj = specification(set,type)
%    obj = specification(list,type)
%    obj = specification(func,'custom')
%    obj = specification(set,type,time)
%    obj = specification(list,type,time)
%    obj = specification(func,'custom',time)
%
% Inputs:
%    set - contSet object that defines the specification
%    type - string that defines the type of spefication:
%               - 'unsafeSet' (default)
%               - 'safeSet'
%               - 'invariant' 
%               - 'custom'
%    list - cell-array storing with contSet objects for multiple parallel
%           specifications
%    func - function handle to a user provided specification check function
%
% Outputs:
%    obj - generated specification object
%
% Example:
%    h = halfspace([1,2],0);
%    spec = specification(h,'unsafeSet');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: reach

% Author:       Niklas Kochdumper
% Written:      29-May-2020             
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

properties (SetAccess = private, GetAccess = public)
    
    % contSet object that corresponds to the specification
    set = [];
    
    % time interval in which the specification is active
    time interval = []; 
    
    % type of specification
    type (1,:) char {mustBeMember(type, ...
         {'unsafeSet','safeSet','invariant','custom'})} = 'unsafeSet';
end
    
methods
    
    % class constructor
    function obj = specification(varargin)
        
        % parse input arguments
        if nargin == 0
            throw(CORAerror('CORA:notEnoughInputArgs',1));
        elseif nargin == 1
            if iscell(varargin{1})
                obj = repelem(obj,length(varargin{1}),1);
                for i = 1:length(varargin{1})
                   obj(i,1).set = varargin{1}{i};
                end
            else
                obj.set = varargin{1};
            end
        elseif nargin == 2 || nargin == 3
            if iscell(varargin{1})
                obj = repelem(obj,length(varargin{1}),1);
                for i = 1:length(varargin{1})
                   obj(i,1).set = varargin{1}{i};
                   obj(i,1).type = varargin{2};
                   if nargin == 3
                      obj(i,1).time = varargin{3}; 
                   end
                end
            else
                obj.set = varargin{1};
                obj.type = varargin{2};
                if nargin == 3
                    obj.time = varargin{3};
                end
            end   
        else
            throw(CORAerror('CORA:tooManyInputArgs',3));
        end  
    end   
    
    % assign array elements
    function obj = subsasgn(obj,S,value)
        % call built-in function
        obj = builtin('subsasgn',obj,S,value);
    end
    
    % get array entries
    function res = subsref(obj,S)
        % call built-in function
        res = builtin('subsref',obj,S);
    end

end
end

%------------- END OF CODE --------------