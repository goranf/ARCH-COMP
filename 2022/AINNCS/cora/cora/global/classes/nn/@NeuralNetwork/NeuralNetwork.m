classdef NeuralNetwork
    % NeuralNetwork - class that stores layer-based neural networks
    %
    % Syntax:
    %    obj = NeuralNetwork(layers)
    %
    % Inputs:
    %    layers - cell-array storing layers of type NNLayer
    %
    % Outputs:
    %    obj - generated object
    %
    % Example:
    %    layers = cell(2, 1)
    %    W = rand(1,10); b = rand(1,1);
    %    layers{1} = NNLinearLayer(W, b)
    %    layers{2} = NNSigmoidLayer()
    %    nn = neuralNetwork(layers);
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    % See also: NNLayer
    %
    % Author:       Tobias Ladner
    % Written:      28-March-2022
    % Last update:  30-March-2022
    % Last revision:---

    %------------- BEGIN CODE --------------

    properties
        layers = []

        neurons_in
        neurons_out
    end
    methods
        % constructor
        function obj = NeuralNetwork(layers)
            obj.layers = layers;

            for i = 1:size(obj.layers, 1)
                [nin, ~] = obj.layers{i}.getNumNeurons();
                if ~isempty(nin)
                    obj.neurons_in = nin;
                    break;
                end
            end

            for i = size(obj.layers, 1):-1:1
                [~, nout] = obj.layers{i}.getNumNeurons();
                if ~isempty(nout)
                    obj.neurons_out = nout;
                    break;
                end
            end
        end

        % methods
        r = evaluate(obj, input, evParams)
        refine(obj, max_order, type, method, x, verbose, force_bounds)
        refinable_layers = getRefinableLayers(obj)
        S = calcSensitivity(obj, x)
        resetApproxOrder(obj)
    end

    methods (Static)
        obj = getRandom(neurons_in, neurons_out, num_layers, activation)

        % conversions methods
        obj = readNetwork(file_path)
        obj = readONNXNetwork(file_path, verbose, inputDataFormats, outputDataFormats)
        obj = readNNetNetwork(file_path)
        obj = readYMLNetwork(file_path)
        obj = readSherlockNetwork(file_path)
        obj = convertDLToolboxNetwork(dltoolbox_layers, verbose)

        obj = getFromOldNeuralNetwork(nn_old)
    end
end