function refine(obj, max_order, type, method, x, verbose, force_bounds)
% refine - refines the network using the maximum error bound
%
% Syntax:
%    res = refine(obj, max_order)
%
% Inputs:
%    obj - object of class NeuralNetwork
%    max_order - maximum order for refinement
%    type - "layer"- or "neuron"-wise refinement
%    method - refinement method
%       "approx_error", "sensitivity", "both"
%    x - point used for sensitivity analysis
%    verbose - whether additional information should be displayed
%    force_bounds - when bounds should be re-calculated
%
% Outputs:
%    None
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: NeuralNetwork/evaluate with 'adaptive'
%
% Author:       Tobias Ladner
% Written:      28-March-2022
% Last update:  14-April-2022 (sensitivity)
% Last revision:---

%------------- BEGIN CODE --------------

% validate parameters
if nargin < 3
    type = "layer";
end
if nargin < 4
    method = "approx_error";
end
if nargin < 5 && (method == "sensitivity" || method == "both")
    error("No point for sensitivity analysis provided.")
end
if nargin < 6
    verbose = false;
end
if nargin < 7
    force_bounds = [];
end


% get refinable layers
refinable_layers = obj.getRefinableLayers();

if (method == "sensitivity" || method == "both")
    % calculate sensitivity
    obj.calcSensitivity(x);
    for i = 1:length(refinable_layers)
        layer_i = refinable_layers{i};
        if method == "sensitivity"
            layer_i.refine_metric = vecnorm(layer_i.sensitivity, 2, 1)';
        elseif method == "both"
            layer_i.refine_metric = layer_i.refine_metric .* vecnorm(layer_i.sensitivity, 2, 1)';
        end
    end
end

% --- LAYER-WISE REFINEMENT ---
if type == "layer"
    % determine most sensible choice for refinement
    metrics = zeros(length(refinable_layers), 3);
    for i = 1:length(refinable_layers)
        layer_i = refinable_layers{i};
        % reduce with norm (TODO try max?)
        metrics(i, 1) = norm(layer_i.refine_metric);
        metrics(i, 2) = i; % layer i
        metrics(i, 3) = max(layer_i.order); % order in layer i
    end

    % filter and sort
    metrics = metrics(metrics(:, 3) < max_order, :);
    metrics = metrics(metrics(:, 1) > 0, :);
    metrics = sortrows(metrics, 1, "descend");

    if size(metrics, 1) > 0
        % refine
        for i = 1:size(metrics, 1)
            layer_i = refinable_layers{metrics(i, 2)};
            order_i = layer_i.order;
            if order_i < max_order
                if verbose
                    fprintf("Refined layer %d from order %d to %d!\n", metrics(i, 2), max(order_i), max(order_i+1))
                end
                order_i = order_i + 1;
                layer_i.order = order_i;

                if any(max(order_i) == force_bounds)
                    % force re-calculation of bounds in all following layers
                    for j = (metrics(i, 2) + 1):length(refinable_layers)
                        layer_j = refinable_layers{j};
                        layer_j.l = [];
                        layer_j.u = [];
                    end
                end

                break;

            end
        end
    else
        if verbose
            fprintf("No layers are left to refine! Either max_order=%d reached or not refineable.\n", max_order);
        end
    end

    % --- NEURON-WISE REFINEMENT ---
elseif type == "neuron"
    % determine most sensible choice for refinement
    metrics = zeros(0, 4);
    for i = 1:length(refinable_layers)
        layer_i = refinable_layers{i};
        metrics_i = layer_i.refine_metric;
        l = size(metrics_i, 1);

        metrics_i(:, 2) = ones(l, 1) * i; % layer i
        metrics_i(:, 3) = (1:l)'; % neuron in layer i
        metrics_i(:, 4) = layer_i.order; % order in layer i

        metrics = [metrics; metrics_i];
    end

    % filter and sort
    metrics = metrics(metrics(:, 4) < max_order, :);
    metrics = metrics(metrics(:, 1) > 0, :);
    metrics = sortrows(metrics, 1, "descend");

    if size(metrics, 1) > 0
        M_max = metrics(1, 1);
        metrics = metrics(metrics(:, 1) > 0.5*M_max, :);

        % refine
        for i = 1:size(metrics, 1)
            layer_i = refinable_layers{metrics(i, 2)};
            order_i = layer_i.order(metrics(i, 3));
            M = metrics(i, 1);

            if verbose
                fprintf("Refined neuron %d from layer %d from order %d to %d!\n", metrics(i, 3), metrics(i, 2), order_i, order_i+1)
            end

            order_i = order_i + 1;
            layer_i.order(metrics(i, 3)) = order_i;

            if any(order_i == force_bounds)
                % force re-calculate bounds in all following layers
                for j = (metrics(i, 2) + 1):length(refinable_layers)
                    layer_j = refinable_layers{j};
                    layer_j.l = [];
                    layer_j.u = [];
                end
            end
        end
    else
        if verbose
            fprintf("No neurons are left to refine! Either max_order=%d reached or not refineable.\n", max_order);
        end
    end
else
    error("Unknown refinement type '%s'", type)
end

end
