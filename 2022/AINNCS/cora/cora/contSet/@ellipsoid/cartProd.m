function E = cartProd(E1,E2,varargin)
% cartProd - returns an over-approximation for the Cartesian product 
%    between two ellipsoids
%
% Syntax:  
%    E = cartProd(E1,E2)
%    E = cartProd(E1,E2,mode)
%
% Inputs:
%    E1,E2 - ellipsoid object
%    mode  - outer approximation ('o') or inner approximation ('i')
%
% Outputs:
%    E - ellipsoid object
%
% Example: 
%    E1 = ellipsoid.generateRandom('Dimension',2);
%    E2 = ellipsoid.generateRandom('Dimension',2);
%    E = cartProd(E1,E2); 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Victor Gassmann
% Written:      19-March-2021
% Last update:  02-June-2022 (VG: handle empty case)
% Last revision:---

%------------- BEGIN CODE --------------
% check input arguments
mode = setDefaultValues({{'o'}},varargin{:});

% check input arguments
inputArgsCheck({{E1,'att',{'ellipsoid'},{'scalar'}};
                {E2,'att',{'ellipsoid'},{'scalar'}}});

% return other if one is empty
if isempty(E1)
    E = E2;
    return;
elseif isempty(E2)
    E = E1;
    return;
end

if strcmp(mode,'o')
    IntE = cartProd(interval(E1),interval(E2));
    r = rad(IntE);
    q = center(IntE);
    TOL = min(E1.TOL,E2.TOL);
    
    % extract degenerate dimensions
    ind_d = withinTol(r,zeros(size(r)),TOL);
    n_nd = sum(~ind_d);
    
    % construct final ellipsoid
    E = ellipsoid(n_nd*diag(r.^2),q);
else
    throw(CORAerror('CORA:noExactAlg',"Inner approximation not implemented!"));
end


%------------- END OF CODE --------------