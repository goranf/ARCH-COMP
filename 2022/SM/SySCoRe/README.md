The file "SySCoRe (ARCH 2022).zip" contains the Code Ocean compute capsule to reproduce our results. The capsule can also be run using the following DOI: https://doi.org/10.24433/CO.2213659.v1.
Note that running the main script (hence all three tutorials subsequently) can take more than one hour.
