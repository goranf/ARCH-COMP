# Prerequisite

+ Docker should be installed.
	
# How to Run Benchmark Instances

```
bash measure_all
```

# Result

The results of will be stored in `result/result.csv` file.

## Format

|             Tool             |      Model      |      Specification      | Verification Result | Time (s) | Bound |
| :--------------------------: | :-------------: | :---------------------: | :-----------------: | :------: | :---: |
| BACH-interaction/ BACH-mixed | Model directory | Specification directory |    safe/ unsafe     |    -     |   -   |

