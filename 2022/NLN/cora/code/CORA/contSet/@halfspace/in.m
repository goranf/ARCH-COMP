function res = in(hs,S)
% in - determines if a halfspace contains a set or a point
%
% Syntax:  
%    res = in(hs,S)
%
% Inputs:
%    hs - halfspace object
%    S - contSet object or single point
%
% Outputs:
%    res - true/false
%
% Example: 
%    hs = halfspace([-1;-1],0);
%    Z1 = zonotope([3 1 1 0;3 1 0 1]);
%    Z2 = Z1 - [3;3];
%
%    in(hs,Z1)
%    in(hs,Z2)
%
%    figure; hold on;
%    xlim([-6,6]);
%    ylim([-6,6]);
%    plot(hs,[1,2],'b');
%    plot(Z1,[1,2],'FaceColor','g');
%
%    figure; hold on;
%    xlim([-6,6]);
%    ylim([-6,6]);
%    plot(hs,[1,2],'b');
%    plot(Z2,[1,2],'FaceColor','r');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope/in

% Author:       Matthias Althoff, Niklas Kochdumper
% Written:      14-June-2016
% Last update:  27-July-2016
%               02-Sep-2019
%               19-Nov-2019 (NK, extend to all set representations)
% Last revision:---

%------------- BEGIN CODE --------------

% point in halfspace containment
if isnumeric(S)
    tmp = hs.c' * S;
    res = tmp < hs.d | withinTol(tmp,hs.d);
    
% set in halfspace containment
else
    val = supportFunc(S,hs.c,'upper');
    res = val < hs.d | withinTol(val,hs.d);
end

%------------- END OF CODE --------------