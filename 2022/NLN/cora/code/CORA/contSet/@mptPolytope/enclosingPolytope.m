function P = enclosingPolytope(P)
% enclosingPolytope - (dummy function)
%
% Syntax:  
%    P = enclosingPolytope(P)
%
% Inputs:
%    P - mptPolytope object
%
% Outputs:
%    P - mptPolytope object
%
% Example: 
%    ---
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope, interval

% Author:       Matthias Althoff
% Written:      30-July-2016
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% input polytope is already a polytope that encloses itself

%------------- END OF CODE --------------