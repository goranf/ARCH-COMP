function R = mtimes(M,R)
% mtimes - Overloaded '*' operator for the multiplication of a matrix or an
%    with a reachSet object
%
% Syntax:  
%    R = mtimes(M,R)
%
% Inputs:
%    M - numerical matrix
%    R - reachSet object 
%
% Outputs:
%    R - transformed reachset object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: plus

% Author:       Niklas Kochdumper
% Written:      04-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

for i = 1:size(R,1)
    R(i).timeInterval.set = cellfun(@(x) M*x, ...
                            R(i).timeInterval.set,'UniformOutput',false);
    R(i).timePoint.set = cellfun(@(x) M*x, ...
                            R(i).timePoint.set,'UniformOutput',false);             
end

%------------- END OF CODE --------------