#!/usr/bin/env bash
set -e

# install Python
apt update
wget https://www.python.org/ftp/python/3.7.10/Python-3.7.10.tgz
tar -xf Python-3.7.10.tgz
cd Python-3.7.10
./configure
make install

# install CommonRoad driveability checker
python3.7 -m pip install --no-cache-dir pybind11
python3.7 -m pip install --no-cache-dir wheel
python3.7 -m pip install --no-cache-dir setuptools
python3.7 -m pip install --no-cache-dir celluloid
git clone https://gitlab.lrz.de/tum-cps/commonroad-drivability-checker.git 
cd commonroad-drivability-checker
bash build.sh -e /usr/local -v 3.7 --cgal --serializer -w -i -j 8

# install Multi Parametric Toolbox (required for CORA)
cd
mkdir -p toolboxes
cd toolboxes
mkdir tbxmanager && cd tbxmanager
echo "
urlwrite('https://raw.githubusercontent.com/verivital/tbxmanager/master/tbxmanager.m', 'tbxmanager.m');
tbxmanager
tbxmanager install mpt mptdoc cddmex fourier glpkmex hysdel lcp sedumi espresso;
" > install_tbx.m
matlab -nodisplay -r "install_tbx; mpt_init; addpath(genpath('toolboxes')); savepath"