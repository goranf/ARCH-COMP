from src.actors.envs.environment import AbstractEnvironment
from src.network_parser.network_model import NetworkModel
from src.verification.bounds.bounds import HyperRectangleBounds


class PendulumEnv(AbstractEnvironment):

    def __init__(self, sin_model):
        """
        Deterministic environment for the case of a frictionless
        pendulum that is being manipulated by an agent whose aim
        is to keep it upright.
        """

        assert isinstance(sin_model, NetworkModel)

        # set the branching factor to 1
        super(PendulumEnv, self).__init__(1)

        self.sin_model = sin_model

        self.dt = 0.05
        self.g = 1.0
        self.m = 0.5
        self.L = 0.5
        self.c = 0.

    def get_constraints_for_transition(self, i, constrs_manager, action_vars, input_state_vars):
        """
        :param i:
        :param constrs_manager:
        :param action_vars:
        :param input_state_vars: variables representing the current state
        :return:
        """

        if len(input_state_vars) != 2:
            raise Exception("Expecting exactly 2 variables representing an extended observation of environment state "
                            "(angle and angular velocity. "
                            "Instead got {} variables".format(len(input_state_vars)))

        if len(action_vars) != 1:
            raise Exception("Expecting exactly one variable representing an agent's action. "
                            "Instead got {} variables".format(len(action_vars)))

        constrs = []

        [theta, theta_dot] = input_state_vars
        torque = action_vars[0]

        # Get and add the constraint for computing sine of theta
        [sin_theta], sin_constrs = constrs_manager.get_network_constraints(self.sin_model.layers, [theta])
        constrs.extend(sin_constrs)

        # Compute next_theta and next_theta_dot bounds
        bounds = constrs_manager.get_variable_bounds([theta, theta_dot, torque])
        sin_theta_bounds = constrs_manager.get_variable_bounds([sin_theta])

        next_theta_lower, next_theta_upper = self.next_theta_bounds(bounds.get_lower(), bounds.get_upper())
        next_theta_dot_lower, next_theta_dot_upper = \
            self.next_theta_dot_bounds(bounds.get_lower() + sin_theta_bounds.get_lower(),
                                       bounds.get_upper() + sin_theta_bounds.get_upper())

        # Create next theta and next theta dot variables with computed bounds
        [next_theta, next_theta_dot] = \
            constrs_manager.create_state_variables(2,
                                                   lbs=[next_theta_lower, next_theta_dot_lower],
                                                   ubs=[next_theta_upper, next_theta_dot_upper])
        constrs_manager.add_variable_bounds([next_theta, next_theta_dot],
                                            HyperRectangleBounds([next_theta_lower, next_theta_dot_lower],
                                                                 [next_theta_upper, next_theta_dot_upper]))


        # Add the constraints for computing next_theta and next_theta_dot
        # next_theta == theta + theta_dot * self.dt
        # x1(t+1) = x1(t) + x2(t) * dt
        constrs.append(constrs_manager.get_linear_constraint([next_theta, theta, theta_dot],
                                                             [-1, 1, self.dt],
                                                             0))
        # next_theta_dot == \
        #     (1 - self.dt * self.c / (self.m * self.L**2) ) * theta_dot +
        #     self.dt * self.g / self.L * sin_theta +
        #     self.dt / (self.m * self.L**2) * torque
        # x2(t+1) = (1 - dt * c/(m * L^2)) * x2(t) + dt * g/L * sin(x1(t)) + dt/(m * L^2) * T(t)
        constrs.append(
            constrs_manager.get_linear_constraint([next_theta_dot, theta_dot, sin_theta, torque],
                                                  [-1, (1 - self.dt * self.c / (self.m * self.L**2)),
                                                   self.dt * self.g / self.L, self.dt / (self.m * self.L**2)],
                                                  0))

        return [next_theta, next_theta_dot], constrs

    def next_theta_bounds(self, input_lower, input_upper):
        """
        """
        # x1(t+1) = x1(t) + x2(t) * dt
        upper = input_upper[0] + self.dt * input_upper[1]
        lower = input_lower[0] + self.dt * input_lower[1]
        return lower, upper

    def next_theta_dot_bounds(self, input_lower, input_upper):
        """
        input_lower and input_upper contain the bounds of
        theta, theta_dot, torque, sin_theta
        """
        # x2(t+1) = (1 - dt * c/(m * L^2)) * x2(t) + dt * g/L * sin(x1(t)) + dt/(m * L^2) * T(t)
        upper = (1 - self.dt * self.c / (self.m * self.L**2)) * input_upper[1] + \
                self.dt * self.g / self.L * input_upper[3] + \
                self.dt / (self.m * self.L**2) * input_upper[2]

        lower = (1 - self.dt * self.c / (self.m * self.L**2)) * input_lower[1] + \
                self.dt * self.g / self.L * input_lower[3] + \
                self.dt / (self.m * self.L**2) * input_lower[2]

        return lower, upper

