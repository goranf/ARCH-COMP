import math

import numpy as np
import sys

sys.path.append(".")

from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten, Dropout

np.random.seed(7)

xsin_model = Sequential()
xsin_model.add(Dense(16, input_shape=(1,)))
xsin_model.add(Activation('relu'))
xsin_model.add(Dense(16))
xsin_model.add(Activation('relu'))
xsin_model.add(Dense(8))
xsin_model.add(Activation('relu'))
xsin_model.add(Dense(1))
xsin_model.compile(optimizer='adam', loss='mse', metrics=['mae'])
print(xsin_model.summary())

training_count = 20000
xs, ys = [], []
x_lower_bound = -2
x_upper_bound = 2
step = (x_upper_bound - x_lower_bound) / training_count
for i in range(training_count):
    x = x_lower_bound + step * i
    xs.append(np.array([x]))
    ys.append(np.array([math.sin(x)]))

validation_count = 5000
xs_valid, ys_valid = [], []
for i in range(validation_count):
    x = np.random.uniform(x_lower_bound, x_upper_bound)
    xs_valid.append(np.array([x]))
    ys_valid.append(np.array([math.sin(x)]))

xsin_model.fit(np.array(xs), np.array(ys), batch_size=32, validation_data=(np.array(xs_valid), np.array(ys_valid)), epochs=200)

score = xsin_model.evaluate(np.array(xs_valid), np.array(ys_valid), verbose=2)
xsin_model.save('x_sin.h5')

print("Test loss:", score[0])
print("Test mae:", score[1])
