from src.actors.agents.agent import Agent
from src.verification.bounds.bounds import HyperRectangleBounds


class AccAgent(Agent):

    def __init__(self, controller_model):
        """
        """
        # The dimensionality of the action space,
        # it is 1 as the action is the advisory.
        self.ACTION_SPACE_DIM = 1

        # the networks themselves
        self.controller_model = controller_model

        self.Tgap =1.4
        self.Vset = 30

        super(AccAgent, self).__init__()

    def get_constraints_for_action(self, constrs_manager, input_state_vars):
        """
        Create constraints for performing an action. Constraints are only added to the model by
        the caller, to reduce side-effects in this function.
        :param constrs_manager: Manager of constraints.
        :param input_state_vars: variables representing the input state passed to the agent.
        :return: Singleton list of variables.
        :side-effects: Modifies constraints manager when adding variables.
        """

        # Initialise a list of constraints to be added to allow only one advisory network to be used at a given time.
        constrs_to_add = []

        [xlead, vlead, glead, xego, vego, gego] = input_state_vars

        input_state_bounds = constrs_manager.get_variable_bounds(input_state_vars)
        input_lower = input_state_bounds.get_lower()
        input_upper = input_state_bounds.get_upper()

        # bounds for the new variables we need to create,
        # vset, Tgap, Drel and vrel
        # Drel = xlead - xego
        # vrel = vlead - vego
        new_vars_controller_input_lower = [self.Vset, self.Tgap, input_lower[0] - input_upper[3], input_lower[1] - input_upper[4]]
        new_vars_controller_input_upper = [self.Vset, self.Tgap, input_upper[0] - input_lower[3], input_upper[1] - input_lower[4]]

        new_vars_for_controller = constrs_manager.create_state_variables(
            4, lbs=new_vars_controller_input_lower, ubs=new_vars_controller_input_upper)
        constrs_manager.add_variable_bounds(new_vars_for_controller,
                                            HyperRectangleBounds(new_vars_controller_input_lower, new_vars_controller_input_upper))

        [vset, Tgap, Drel, vrel] = new_vars_for_controller
        constrs_to_add.append(
            constrs_manager.get_linear_constraint([Drel, xlead, xego], [-1, 1, -1], 0)
        )
        constrs_to_add.append(
            constrs_manager.get_linear_constraint([vrel, vlead, vego], [-1, 1, -1], 0)
        )

        network_input_vars = [vset, Tgap, vego, Drel, vrel]
        q_vars, network_constrs = constrs_manager.get_network_constraints(
            self.controller_model.layers, network_input_vars)

        constrs_to_add.extend(network_constrs)

        return q_vars, constrs_to_add

