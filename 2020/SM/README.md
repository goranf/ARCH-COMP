# Repeatability evaluation for ARCH-COMP Category Report: Stochastic Models

We describe the repeatability evaluation efforts conducted as part of the ARCH 2020 friendly competition in
the Stochastic Models group. We reported the performance of the tools on various benchmarks on a common 
machine, an instance generated on Amazon Web Services (AWS) server. Some of the participating tools also 
prepared repeatability packages in form Dockerfiles or Code Ocean capsules.

**Acknowledgements**: The repeatability experiment was funded by Amazon Web Services.



The following table provides links to the participating tools, the corresponding code repositories, and the repeatability packages.

| Tool | Tool website | Code repository used in benchmarks | Repeatability package: Dockerfile | Repeatability package: Code Ocean capsule  | Language |
|---|---|---|---|---|---|
| `SReachTools`| https://sreachtools.github.io | https://github.com/sreachtools/ARCH2020 | N/A | https://doi.org/10.24433/CO.5339956.v1 | MATLAB |
| `AMYTISS`| https://github.com/mkhaled87/pFaces-AMYTISS | https://github.com/mkhaled87/pFaces-AMYTISS/tree/master/examples/arch20_artifact | https://raw.githubusercontent.com/mkhaled87/pFaces-AMYTISS/master/Dockerfile | N/A | C++ | 
| `StocHy`| https://github.com/natchi92/stochy | https://github.com/natchi92/stochy/tree/master/src/case_studies | N/A | N/A | C++ |
| `FAUST^2` | http://sourceforge.net/projects/FAUST2 | https://github.com/mkhaled87/ARCH20-FAUST/ | N/A | N/A | MATLAB |
| `Mascot-SDS` | https://gitlab.mpi-sws.org/kmallik/mascot-sds | https://gitlab.mpi-sws.org/kmallik/mascot-sds/-/tree/master/examples/hscc20/vanderpol | N/A | N/A | C++ |


## Repeatability instructions for AWS server

We now describe the steps to reproduce the testing environment on the AWS server platform. 

1. Login to your AWS account from: https://aws.amazon.com/
1. We will use an AWS machine with one of the following AWS instances types
    - `CPU_2`: c5.18xlarge (*All participating tools ran their code on this machine*)
    - `GPU_1`: p3.2xlarge 
    - `CPU_1`: m4.2xlarge
1. In AWS EC2 portal, make sure you have a valid key-pair and create a new instance with the following configurations
    - In Amazon Machine Images (AMI), search for and select “Ubuntu Server 18.04 LTS” or search with the AMI ID (ami-003634241a8fcdec0).
    - For instance type, select one of the instance types from step 2.
    - Leave “Instance Details” to the defaults.
    - For storage, select a minimum size of 20 GB
    - No tags are needed
    - Create a security group with SSH port (22) open and allow all incoming connections
    - Launch the machine using your key-pair. It will be added to the “running instances“ in the EC2 portal.
1. Connect to that public IP using any SSH client and the key file
1. In case you launched a machine with instance type p3.2xlarge, you need to install the NVIDIA GPU driver only for the first time. Refer to the following link for the complete steps to setup the GPU driver: https://linuxconfig.org/how-to-install-the-nvidia-drivers-on-ubuntu-18-04-bionic-beaver-linux


### Setup instructions for `SReachTools`
To run `SReachTools`, see https://sreachtools.github.io/installation for detailed installation instructions. `SReachTools` has the following dependencies:
1. `MATLAB` with `Statistics and Machine Learning Toolbox` (Tested on 2020a)
1. `GUROBI 9.0.2`
1. `CVX 2.2`
1. `MPT3`

### Setup instructions for `AMYTISS`
To run `AMYTISS`, you can use a local machine with similar HW or one of the reported AWS machines. Then, you will either need to build `AMYTISS` from the source code or use a Docker file. See https://github.com/mkhaled87/pFaces-AMYTISS/tree/master/examples/arch20_artifact for more details.

### Setup instructions for `StocHy`
To run `StocHy`, see https://github.com/natchi92/stochy/blob/master/README.md for detailed instructions. Specifically, installation requires running the following scripts `./get_dep.sh` (to get the required dependencies) and `./build_release.sh` (to compile the binaries).

In the folder https://github.com/natchi92/stochy/tree/master/src/case_studies, compile `case_study_arch2020.cpp` to generate the binary corresponding to the case studies in the ARCH 2020 report. Executing this binary provides the option of running any one of the four case studies that were used for the report

### Setup instructions for `FAUST^2`
`FAUST^2` can be downloaded from SourceForge using the following link: http://sourceforge.net/projects/FAUST2. `FAUST^2` only requires MATLAB to work (Tested on 2020a).
Once FAUST is downloaded and unpacked, follow the installation instruction in the README file in the root directory.

### Setup instructions for `MASCOT-SDS`
`MASCOT-SDS` can be downloaded from https://gitlab.mpi-sws.org/kmallik/mascot-sds. `MASCOT-SDS` has the following requirements:
1. `CUDD` library for managing BDDs
1. `MATLAB` for visualization (Tested on 2020a)
1. `GCC` compiler (Linux) or `Xcode` (Mac OS) for MEX-file compilation (see https://www.mathworks.com/support/requirements/supported-compilers.html)

Once MASCOT-SDS is downloaded and unpacked, follow the installation instruction in the README file in the root directory.

 
