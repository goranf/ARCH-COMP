Hylaa repeatability package for 2020 ARCH-Comp Linear System Group, prepared by Stanley Bak.

Instructions:

Build the container
> docker build . -t hylaa


Run all measurements (results printed to stdout; should match expected_stdout.txt), takes about an hour:
> docker run hylaa
