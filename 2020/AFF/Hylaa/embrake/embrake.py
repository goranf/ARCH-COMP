'''
electro-mechanical brake

from ARCHCOMP20: https://cps-vo.org/node/67351

'''

import math
import time

from matplotlib import collections

import numpy as np

from hylaa.hybrid_automaton import HybridAutomaton
from hylaa.settings import HylaaSettings, PlotSettings
from hylaa.aggstrat import Aggregated
from hylaa.core import Core
from hylaa.stateset import StateSet
from hylaa import lputil

def define_ha():
    '''make the hybrid automaton'''

    ha = HybridAutomaton()

    L = 0.001
    K_P = 10000
    K_I = 1000
    R = 0.5
    K = 0.02
    d_rot = 0.1
    i = 113.1167
    T_sample = 0.0001
    x_0 = 0.05

    #I' == 1/L * K_P * x_e + 1/L * K_I * x_c - 1/L * R * I - 1/L * K * K / d_rot * I &amp;
    #x' == K / i / d_rot * I &amp;
    #T' == 1 &amp;
    #x_e' == 0 &amp;
    #x_c' == 0

    # variables: I, x, T, x_c, x_e, affine
    # x_c and x_e get assigned on the reset... maybe it's not worth modeling them this way though

    row_I = [- (1/L * R + 1/L * K * K / d_rot), 0, 0, 1/L * K_I, 1/L * K_P, 0]
    row_x = [K / i / d_rot, 0, 0, 0, 0, 0]
    row_T = [0, 0, 0, 0, 0, 1]
    row_xe = [0, 0, 0, 0, 0, 0]
    row_xc = [0, 0, 0, 0, 0, 0]
    row_affine = [0, 0, 0, 0, 0, 0]
    
    a_mat = [row_I, row_x, row_T, row_xe, row_xc, row_affine]

    one = ha.new_mode('one')
    one.set_dynamics(a_mat)
    one.set_invariant([[0, 0, 1, 0, 0, 0]], [T_sample]) # t <= t_sample

    # reset assignment:
    #T' == 0 &amp;
    #x_e' == x_0 - x &amp;
    #x_c' == x_c + T_sample * (x_0 - x)
    t = ha.new_transition(one, one)
    t.set_guard([[0, 0, -1, 0, 0, 0],], [-T_sample]) # t >= t_sample

    mat = np.identity(6)
    mat[2, :] = 0 # T := 0
    mat[3, :] = [0, -1, 0, 0, 0, x_0] # x_0 - x
    mat[4, :] = [0, -T_sample, 0, 1, 0, T_sample * x_0] #
    t.set_reset(mat)

    #def set_reset(self, reset_csr=None, reset_minkowski_csr=None, reset_minkowski_constraints_csr=None,
    #              reset_minkowski_constraints_rhs=None):

    error = ha.new_mode('error')
    t = ha.new_transition(one, error)

    # x >= x_0
    unsafe_rhs = []
    t.set_guard([0, -1, 0, 0, 0, 0], [-x_0])

    return ha

def make_init(ha):
    '''returns list of initial states'''

    mode = ha.modes['one']
    # variables: I, x, T, x_c, x_e, affine
    
    init_lpi = lputil.from_box([[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [1, 1]], mode)

    init_list = [StateSet(init_lpi, mode)]

    return init_list

def define_settings():
    'get the hylaa settings object'

    step = 0.0001 # T_sample = 0.0001
    max_time = 0.1
    settings = HylaaSettings(step, max_time)

    #settings.process_urgent_guards = True
    #settings.aggstrat.deaggregate = True # use deaggregation
    #settings.aggstrat.deagg_preference = Aggregated.DEAGG_LEAVES_FIRST
    #settings.aggstrat.agg_type = Aggregated.AGG_CONVEX_HULL

    plot_settings = settings.plot
    plot_settings.plot_mode = PlotSettings.PLOT_NONE

    plot_settings.label.x_label = '$t$'
    plot_settings.label.y_label = '$x$'

    #settings.plot.extra_collections = cols

    plot_settings.label.title = 'Deterministic Embrake (Hylaa)'

    return settings

def run_hylaa():
    'Runs hylaa with the given settings'

    ha = define_ha()
    settings = define_settings()
    init_states = make_init(ha)

    start = time.perf_counter()
    result = Core(ha, settings).run(init_states)
    assert not result.has_concrete_error, "error mode was reachable"
    diff = time.perf_counter() - start

    with open("../results.csv", "a") as f:
        f.write(f"Hylaa,BRKD,C01,1,{diff},\n")

if __name__ == '__main__':
    run_hylaa()
