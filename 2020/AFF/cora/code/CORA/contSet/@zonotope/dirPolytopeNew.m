function Zenclose = dirPolytopeNew(Z,direction)
% dirPolytopeNew - ?
%
% Syntax:  
%    Zenclose = dirPolytopeNew(Z,direction)
%
% Inputs:
%    Z - zonotope object
%    direction - vector
%
% Outputs:
%    Zenclose - enclosing zonotope object
%
% Example: 
%    Z = zonotope([0;0],[1 -2 0 3 2; -1 0 3 -2 4]);
%    direction = [1;0];
%    Zenclose = dirPolytopeNew(Z,direction);
% 
%    plot(Z); hold on;
%    plot(Zenclose,[1,2],'r');
%
% Other m-files required: vertices, polytope
% Subfunctions: none
% MAT-files required: none
%
% See also: interval,  vertices

% Author:        Matthias Althoff
% Written:       14-October-2008
% Last update:   ---
% Last revision: ---

%------------- BEGIN CODE --------------

d=length(direction);
orient=eye(d);

newGen=direction/norm(direction);

%retrieve most aligned generator from orient
for iGen=1:length(orient(1,:))
    h(iGen)=abs(newGen'*orient(:,iGen)/norm(orient(:,iGen)));
end

[val,ind]=sort(h);
pickedIndices=ind(1:(end-1));

newP=[newGen,orient(:,pickedIndices)];

%compute oriented rectangular hull
Ztrans=inv(newP)*Z;
IHtrans=interval(Ztrans);
Ztrans=zonotope(IHtrans);
Zenclose=newP*Ztrans;
 
%------------- END OF CODE --------------