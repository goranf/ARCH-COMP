function val = query(R,prop)
% query - get properties of the reachable set
%
% Syntax:  
%    val = query(R,prop)
%
% Inputs:
%    R - reachSet object
%    prop - property ('reachSet')
%
% Outputs:
%    val - value of the property
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: reachSet

% Author:       Niklas Kochdumper
% Written:      02-June-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    if strcmp(prop,'reachSet')
        val = R(1,1).timeInterval.set;
        for i = 2:size(R,1)
           val = [val;R(i).timeInterval.set]; 
        end
    else
       error('Wrong value for input arguments "prop"!'); 
    end

%------------- END OF CODE --------------