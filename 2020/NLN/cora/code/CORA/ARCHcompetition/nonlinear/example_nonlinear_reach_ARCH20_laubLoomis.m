function text = example_nonlinear_reach_ARCH20_laubLoomis()
% example_nonlinear_reach_ARCH20_laubLoomis - example of 
% nonlinear reachability analysis
%
% Syntax:  
%    example_nonlinear_reach_ARCH20_laubLoomis
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% 
% Author:       Matthias Althoff, Niklas Kochdumper
% Written:      27-March-2019
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------


dim=7;

% Parameter ---------------------------------------------------------------

params.tFinal = 20;
x0 = [1.2; 1.05; 1.5; 2.4; 1; 0.1; 0.45];


% Reachability Settings ---------------------------------------------------

options.zonotopeOrder = 200;
options.intermediateOrder = 20;
options.taylorTerms = 4;

options.simplify = 'optimize';



% Reachability Analysis ---------------------------------------------------

R = cell(3,1);

% Initial set W = 0.1
W = 0.1;
params.R0 = polyZonotope(x0,W*eye(dim));

options1 = options;
options1.timeStep = 0.02;
options1.errorOrder = 5;
options1.alg = 'poly';
options1.tensorOrder = 3;

options1.maxPolyZonoRatio = 0.1;
options1.restructureTechnique = 'reduceFullGirard';
options1.maxDepGenOrder = 8;

fun1 = @(x,u) sevenDimNonlinEq(x,u);
sys = nonlinearSys(7,1,fun1,options1); 

tic
R{1} = reach(sys, params, options1);
tComp1 = toc;

width1 = 2*rad(interval(project(R{1}{end}{1},4)));
disp(['computation time of reachable set (W=0.1): ',num2str(tComp1)]);
disp(['width of final reachable set (W=0.1): ',num2str(width1)]);
disp(' ');


% Initial set W = 0.05
W = 0.05;
params.R0 = zonotope([x0,W*eye(dim)]);

options2 = options;
options2.timeStep = 0.025;
options2.errorOrder = 1;
options2.alg = 'lin';
options2.tensorOrder = 3;

fun2 = @(x,u) sevenDimNonlinEq(x,u);
sys = nonlinearSys(7,1,fun2,options2); 

tic
R{2} = reach(sys, params, options2);
tComp2 = toc;

width2 = 2*rad(interval(project(R{2}{end}{1},4)));
disp(['computation time of reachable set (W=0.05): ',num2str(tComp2)]);
disp(['width of final reachable set (W=0.05): ',num2str(width2)]);
disp(' ');


% Initial set W = 0.01
W = 0.01;
params.R0 = zonotope([x0,W*eye(dim)]);

options3 = options;
options3.timeStep = 0.1;
options3.errorOrder = 1;
options3.tensorOrder = 2;
options3.alg = 'lin';

fun3 = @(x,u) fun1(x,u);
sys = nonlinearSys(7,1,fun3,options3); 

tic
R{3} = reach(sys, params, options3);
tComp3 = toc;

width3 = 2*rad(interval(project(R{3}{end}{1},4)));
disp(['computation time of reachable set (W=0.01): ',num2str(tComp3)]);
disp(['width of final reachable set (W=0.01): ',num2str(width3)]);
disp(' ');



% visualization -----------------------------------------------------------

colors = {'b',[0,0.6,0],'r'};

figure
hold on

% plot results over time
for k = 1:length(R)
    
    t = 0;
    timeStep = params.tFinal/length(R{k});

    for i=1:length(R{k})
        for j=1:length(R{k}{i}) 
            
            % construct interval
            intX = interval(project(R{k}{i}{j},4));
            intT = interval(t,t+timeStep);
            int = cartProd(intT,intX);
            
            % plot the interval
            plotFilled(int,[1 2],colors{k},'EdgeColor','none');
            
            t = t + timeStep;
        end
    end
end

box on
xlabel('t')
ylabel('x_4')
axis([0,20,1.5,5]);

text{1} = ['CORA,LALO20,W001,1,',num2str(tComp1),',',num2str(width1)];
text{2} = ['CORA,LALO20,W005,1,',num2str(tComp2),',',num2str(width2)];
text{3} = ['CORA,LALO20,W01,1,',num2str(tComp3),',',num2str(width3)];


%------------- END OF CODE --------------