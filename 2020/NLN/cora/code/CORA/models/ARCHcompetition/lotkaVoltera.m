function HA = lotkaVoltera(options)




    % Location Outside ----------------------------------------------------

    % dynamics
    sys = nonlinearSys(4,1,@lotkaVolteraOutside,options);

    % invariant set
    syms x y c t
    vars = [x;y;c;t];
    eq = -(x-1)^2 - (y-1)^2 + 0.15^2;

    inv = levelSet(eq,vars,'<=');

    % transition
    guard = levelSet(eq,vars,'==');
    reset.A = eye(4); reset.b = zeros(4,1);

    trans{1} = transition(guard, reset, 2);

    % location
    loc{1} = location('outside',1, inv, trans, sys);


    % Location Inside -----------------------------------------------------

    % dynamics
    sys = nonlinearSys(4,1,@lotkaVolteraInside,options);

    % invariant set
    syms x y c t
    vars = [x;y;c;t];
    eq = (x-1)^2 + (y-1)^2 - 0.15^2;

    inv = levelSet(eq,vars,'<=');

    % location
    loc{2} = location('inside',2, inv, [], sys);


    % Hybrid Automaton ----------------------------------------------------

    HA = hybridAutomaton(loc);
