function main()

    addpath(genpath('../code'));


    % Install MPT Toolbox ------------------------------------------------------------

    % create installation directory
    mkdir('tbxmanager')
    cd tbxmanager

    % install toolbox-manager
    urlwrite('http://www.tbxmanager.com/tbxmanager.m', 'tbxmanager.m');
    tbxmanager
    savepath

    % install mpt-toolbox
    tbxmanager install mpt mptdoc cddmex fourier glpkmex hysdel lcp sedumi espresso

    % initialize toolbox
    mpt_init
    cd ..

    % initialize .csv file and track command-line outputs
    diary '../results/computationTime'
    fid = fopen('../results/results.csv','w');
    

    disp('Production-Destruction ---------------------------------------------------------');

    text = example_nonlinear_reach_ARCH20_prodDes;
    fprintf(fid,'%s\n',text{1});
    fprintf(fid,'%s\n',text{2});
    fprintf(fid,'%s\n',text{3});
    saveas(gcf, '../results/prodDes.png');
    close(gcf);
    
    disp(' ')
    disp(' ')


    disp('Van-der Pol --------------------------------------------------------------------');
    disp(' ');

    text = example_nonlinear_reach_ARCH20_vanDerPol;
    fprintf(fid,'%s\n',text{1});
    fprintf(fid,'%s\n',text{2});
    saveas(gcf, '../results/vanDerPol.png');
    close(gcf);
    
    disp(' ')
    disp(' ')
    
    
    disp('Laub Loomis --------------------------------------------------------------------');
    disp(' ');

    text = example_nonlinear_reach_ARCH20_laubLoomis;
    fprintf(fid,'%s\n',text{1});
    fprintf(fid,'%s\n',text{2});
    fprintf(fid,'%s\n',text{3});
    saveas(gcf, '../results/laubLoomis.png');
    close(gcf);
    
    disp(' ')
    disp(' ')
    
    
    disp('Quadrotor ----------------------------------------------------------------------');
    
    text = example_nonlinear_reach_ARCH20_quadrotor;
    fprintf(fid,'%s\n',text{1});
    fprintf(fid,'%s\n',text{2});
    fprintf(fid,'%s\n',text{3});
    saveas(gcf, '../results/quadrotor.png');
    close(gcf);
    
    disp(' ')
    disp(' ')
    

    disp('Lotka Volterra -----------------------------------------------------------------');
    disp(' ');

    text = example_hybrid_reach_ARCH20_lotkaVolterra;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/lotkaVolterra.png');
    close(gcf);
    
    disp(' ')
    disp(' ')
    
    
    disp('Spacecraft ---------------------------------------------------------------------');
    disp(' ');

    text = example_hybrid_reach_ARCH20_spacecraft;
    fprintf(fid,'%s\n',text);
    saveas(gcf, '../results/spacecraft.png');
    close(gcf);
    
    disp(' ')
    disp(' ')
    
    % Close .csv file
    fclose(fid);
    
end
