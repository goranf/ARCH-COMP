We provide a static binary of HyDRA (bin/hydra-cli) which is called in the script "runAFF.sh" with all benchmarks in
which we participated (navigation, motorcade, distributed controller, ttethernet).

We provide an automated script which executes all benchmarks and organises the output

	run.sh

For manual execution use:

	./bin/hydra-cli -m <modelFile> -r <representation[box,support_function]> -d <time step size [rational]> -t
	./<numThreads>

	e.g.:

	./bin/hydra-cli -m benchmarks/motorcar_5_2.model -r support_function -d 1/1000 -t 3

Note: The parameters for the model file and the state set representation are mandatory, while the time step size
defaults to the value set in the model file and the number of threads defaults to 1.

Note: In this category no plots were produced, the compiled binary does not produce plot outputs.

In case anything crashes, or does not work properly, please contact
	stefan.schupp@cs.rwth-aachen.de
