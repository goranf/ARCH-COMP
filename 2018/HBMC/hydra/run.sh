#!/usr/bin/env bash

mkdir -p output

echo "Run navigation 2x2 model."
./bin/hydra-cli -m benchmarks/NAV_2_2.model -r box
echo "cleanup and processing plots"
mkdir -p output/NAV_2_2
mv *.plt output/NAV_2_2/
mv *.gen output/NAV_2_2/
mv *.gv output/NAV_2_2/
pushd output/NAV_2_2
	#gnuplot *.plt
popd
echo "done."

echo "---------------------------------------------------------------------"

echo "Run navigation 3x3 model."
./bin/hydra-cli -m benchmarks/NAV_3_3.model -r box
echo "cleanup and processing plots"
mkdir -p output/NAV_3_3
mv *.plt output/NAV_3_3/
mv *.gen output/NAV_3_3/
mv *.gv output/NAV_3_3/
pushd output/NAV_3_3
	#gnuplot *.plt
popd
echo "done."

echo "---------------------------------------------------------------------"

echo "Run navigation 4x4 model."
./bin/hydra-cli -m benchmarks/NAV_4_4.model -r box
echo "cleanup and processing plots"
mkdir -p output/NAV_4_4
mv *.plt output/NAV_4_4/
mv *.gen output/NAV_4_4/
mv *.gv output/NAV_4_4/
pushd output/NAV_4_4
	#gnuplot *.plt
popd
echo "done."

echo "---------------------------------------------------------------------"

echo "Run navigation motorcar model."
./bin/hydra-cli -m benchmarks/motorcar_5_2.model -r box
echo "cleanup and processing plots"
mkdir -p output/motorcar_5_2
mv *.plt output/motorcar_5_2/
mv *.gen output/motorcar_5_2/
mv *.gv output/motorcar_5_2/
pushd output/motorcar_5_2
	#gnuplot *.plt
popd
echo "done."

echo "---------------------------------------------------------------------"

echo "Run distributed controller 04 model."
./bin/hydra-cli -m benchmarks/dist_controller_04.model -r box -d 1/10 -t 4
echo "cleanup and processing plots"
mkdir -p output/dist_controller_04
mv *.plt output/dist_controller_04/
mv *.gen output/dist_controller_04/
mv *.gv output/dist_controller_04/
pushd output/dist_controller_04
	#gnuplot *.plt
popd
echo "done."

echo "---------------------------------------------------------------------"

echo "Run navigation TTEthernet model."
./bin/hydra-cli -m benchmarks/TTEthernet_Simplified_5.model -r support_function -t 4 -d 1/25
echo "cleanup and processing plots"
mkdir -p output/TTEthernet_Simplified_5
mv *.plt output/TTEthernet_Simplified_5/
mv *.gen output/TTEthernet_Simplified_5/
mv *.gv output/TTEthernet_Simplified_5/
pushd output/TTEthernet_Simplified_5
	#gnuplot *.plt
popd
echo "done."
