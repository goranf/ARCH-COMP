FalStar: Fast adaptive falsification for hybrid systems
Reproduction package for ARCH2018.


Requirements

    Java 1.8
    MATLAB (the model was saved with R2017a)

Link the MATLAB libraries to ./lib

    ./link $MATLAB_ROOT $ARCH

where $MATLAB_ROOT is the path that ends with eg. R2017a
and $ARCH is they code for your architecture (glnxa64, maci64, win64).


Reproduce the results for the ARCH2018 competition

    ./run-benchmark

This runs the adaptive algorithm and uniform random sampling
(50 trials, with 100 simulations each),
which will take about 5 to 10 minutes.


The results will appear as a new line in

    ./result/afc.csv

and the output of the tool is logged to

    ./result/afc.log



The configuration file is

    ./benchmark/afc.cfg

The model is

    ./benchmark/AbstractFuelControl_M1.slx
    
with the following configuration parameters

    input in [t__,u__]
    output in tout, yout (data structure with time)

    input port 1: throttle (interpolation turned off)
    input port 2: engine   (interpolation turned off)

    output port 1: mu (verification measurement)
    output port 2: mode (0 or 1)
