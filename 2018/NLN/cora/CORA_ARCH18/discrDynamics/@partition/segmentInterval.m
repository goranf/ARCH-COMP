function [intervals]=segmentInterval(Obj,varargin)
% segmentIntervals: returns a cell array of interval objects corresponding
% to the partition segments whose indices are given as input
%
% Input:
% 1st input:            Interval object
% 2nd input:            segment indices as vector
%
% Output:
% intervals:            Cell array of interval objects
%
% Checked:  1.8.17, AP


if nargin > 1   % if the cells are specified
    cellNrs = varargin{1};
    if ~(prod(cellNrs > 0)&&prod(cellNrs<=prod(Obj.nrOfSegments))) % check the demanded cells are within limits...
    disp('some cells are out of bounds');
        cellNrs = cellNrs((cellNrs > 0)&(cellNrs<=prod(Obj.nrOfSegments)));
    end
else
    cellNrs = [1:prod(Obj.nrOfSegments)];
end

% Get subscripts out of the segment number 
subscripts=i2s(Obj,cellNrs);

for i = 1:size(subscripts,1)
    for j = 1:size(subscripts,2)
        leftLimit(j,1) = Obj.dividers{j}(subscripts(i,j));
        rightLimit(j,1) = Obj.dividers{j}(subscripts(i,j)+1);
    end
    intervals{i} = interval(leftLimit, rightLimit);
end

