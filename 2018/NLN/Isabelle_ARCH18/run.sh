#!/usr/bin/env bash
set -x
isabelle/bin/isabelle build -d afp-devel/thys -b HOL-ODE-ARCH-COMP
cd afp-devel/thys/Ordinary_Differential_Equations/Ex
./plot_arch_comp
echo The plots are:
cd ../../../..
ls -l afp-devel/thys/Ordinary_Differential_Equations/Ex/*.pdf
