#!/usr/bin/env bash
set -x
hg clone -r 3db6c9338ec1 https://isabelle.in.tum.de/repos/isabelle isabelle
hg clone -r 001c9cdc7d1d https://bitbucket.org/isa-afp/afp-devel afp-devel
isabelle/bin/isabelle components -I
isabelle/bin/isabelle components -a
(cd afp-devel; patch -p1 < ../write_files.patch)
isabelle/bin/isabelle build -d afp-devel/thys -b HOL-ODE-Numerics
