function HA = test.m(~)


%% Generated on 26-Apr-2018

%---------------Automaton created from Component 'system'------------------

%----------------------Component system.SpaceCraft-------------------------

%-----------------------------State Passive--------------------------------

%% equation:
%   t'==1 & x'==vx & y'==vy & vx'== sqrt(mu/r^3)^2 * x + 2*sqrt(mu/r^3)*vy + mu/r^2 - mu/sqrt((r+x)^2-y^2)^3 * (r+x) & vy'== sqrt(mu/r^3)^2*y - 2*sqrt(mu/r^3)*vx - mu/sqrt((r+x)^2-y^2)^3 * y
dynOpt = struct('tensorOrder',1);
dynamics = nonlinearSys(5,1,@test.m_St1_FlowEq,dynOpt); 

%% equation:
%   x<=-100
invA = ...
[1,0,0,0,0];
invb = ...
[-100];
invOpt = struct('A', invA, 'b', invb);
inv = mptPolytope(invOpt);

trans = {};
loc{1} = location('S1',1, inv, trans, dynamics);



HA = hybridAutomaton(loc);


end