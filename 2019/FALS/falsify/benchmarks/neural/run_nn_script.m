% (C) 2019 National Institute of Advanced Industrial Science and Technology
% (AIST)

% NN Benchmark
%%%%%%%%%%%%%%%%%%%%

tmpl = struct();
tmpl.mdl = 'narmamaglev_v1';
tmpl.input_range = [1 3];
tmpl.output_range = [1 3; 1 3; 0 2];
tmpl.gen_opts = {};
tmpl.interpolation = {'pconst'};
tmpl.option = {};
tmpl.maxEpisodes = maxEpisodes;
tmpl.agentName = 'Falsifier';

%Formula 1
fml = struct(tmpl);
fml.targetFormula = '[]_[1.0,37.0](!close_ref -> <>_[0,2][]_[0,1]close_ref)';
fml.monitoringFormula = '[.]_[30,30](!close_ref -> <>_[0,20][]_[0,10]close_ref)';
fml.preds(1).str = 'close_ref';
fml.preds(1).A = [0 0 1];
fml.preds(1).b = -0.001;
fml.stopTime = 40;


configs = { };
alphas = {0.005};
betas = {0.04};
for i = 1:size(alphas,2)
    for j = 1:size(betas, 2)
        config = struct(fml);
        config.init_opts = {{'u_ts', 0.001}, {'alpha', alphas{i}}, {'beta', betas{j}}};
        config.expName = [num2str(alphas{i}), '-', num2str(betas{j})];
        config.sampleTime = 3;
        for k = 1:size(algorithms, 2)
            config.algoName = algorithms{k};
            for l = 1:maxIter
                configs = [configs, config];
            end
        end
    end
end

for i = 1:size(alphas,2)
    for j = 1:size(betas, 2)
        config = struct(fml);
        config.init_opts = {{'u_ts', 0.001}, {'alpha', alphas{i}}, {'beta', betas{j}}};
        config.expName = [num2str(alphas{i}), '-', num2str(betas{j})];
        config.sampleTime = 14;
        for k = 1:size(algorithms, 2)
            config.algoName = algorithms{k};
            for l = 1:maxIter
                configs = [configs, config];
            end
        end
    end
end


do_experiment('NN', configs);
