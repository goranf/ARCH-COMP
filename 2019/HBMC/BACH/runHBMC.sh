runlim="runlim -s 4096 -t 3600 -r 3600"
path=benchmarks
bach=src/bach

NAV="2 3 4"
for i in ${NAV};
do
	model_name=NAV_${i}_${i}
	log=ARCH2018/${model_name}.bach
	echo "running bach on example ${model_name}"
	k=20
	$runlim -o ARCH2018/${model_name}.runlim ${bach} benchmarks/Navigation/${model_name}.xml benchmarks/Navigation/${model_name}.cfg ${k} > ${log}
done

motor="5 10 15"
for i in ${motor};
do
	model_name=motorcar_${i}
	log=ARCH2018/${model_name}.bach
	echo "running bach on example ${model_name}"
	k=20
	$runlim -o ARCH2018/${model_name}.runlim ${bach} benchmarks/Motorcade/${model_name}.xml benchmarks/Motorcade/${model_name}.cfg ${k} > ${log}
done