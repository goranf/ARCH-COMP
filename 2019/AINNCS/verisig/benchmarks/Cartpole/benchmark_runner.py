#!/usr/bin/python3

import subprocess

verisig_path = '/usr/app/verisig/verisig'
flowstar_path = '/usr/app/verisig/flowstar/flowstar'

legend3 = ['X3_LOWER', 'X3_UPPER']
test_set3 = [
    [-0.05, -0.04],
    [-0.04, -0.03],
    [-0.03, -0.02],
    [-0.02, -0.01],
    [-0.01, 0],
    [0, 0.01],
    [0.01, 0.02],
    [0.02, 0.03],
    [0.03, 0.04],
    [0.04, 0.05]
]

legend4 = ['X4_LOWER', 'X4_UPPER']
test_set4 = [
    [-0.05, -0.04],
    [-0.04, -0.03],
    [-0.03, -0.02],
    [-0.02, -0.01],
    [-0.01, 0],
    [0, 0.01],
    [0.01, 0.02],
    [0.02, 0.03],
    [0.03, 0.04],
    [0.04, 0.05]
]

print("Building the base model...")
subprocess.run([verisig_path, '-o' ,'-nf', 'Cartpole.xml', 'tanh24x48.yml'])

with open('Cartpole.model', 'r') as f:
    model = f.read()

for test3 in test_set3:
    for test4 in test_set4:
        print("=========================================")
        print("Running test with initial conditions of: ")
        test_model = model
        for i in range(len(legend3)):
            print(legend3[i] + '=' + str(test3[i]), end=', ')
            test_model = test_model.replace(legend3[i], str(test3[i]))
        for i in range(len(legend4)):
            print(legend4[i] + '=' + str(test4[i]), end=', ')
            test_model = test_model.replace(legend4[i], str(test4[i]))
        print()
        print("=========================================")

        subprocess.run(flowstar_path, input=test_model, shell=True, universal_newlines=True)
        print()
        print()
