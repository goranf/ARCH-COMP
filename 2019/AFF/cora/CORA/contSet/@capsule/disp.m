function disp(C)
% disp - Displays the center, generator, and radius of a capsule
%
% Syntax:  
%    disp(C)
%
% Inputs:
%    C - capsule
%
% Outputs:
%    ---
%
% Example: 
%    C = capsule([1;1],[1;0],0.5);
%    display(C);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Matthias Althoff
% Written:      04-March-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% display id, dimension
display(C.contSet);

% display center
disp('center: ');
disp(C.c);

% display generator
disp('generator: ');
disp(C.g); 

% display radius
disp('radius: ');
disp(C.r); 

%------------- END OF CODE --------------