function res = example_parallel_hybrid_02_lowPassFilter()
% example_parallel_hybrid_02_lowPassFilter - example for reachability of a
% parallel hybrid automaton. The system consists of two piecewise linear
% low-pass filters that are connected in series
%
% Syntax:  
%    example_parallel_hybrid_02_lowPassFilter
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean, true if completed

% Author:       Niklas Kochdumper
% Written:      06-July-2018
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------

PHA = lowpassFilter();


% Options -----------------------------------------------------------------

% General Options
options.tStart = 0; 
% options.tFinal = 0.8;
options.tFinal = 0.4;
options.taylorTerms = 8; 
options.zonotopeOrder = 9; 
options.polytopeOrder = 10; 
options.errorOrder = 2; 
options.originContained = 1; 
options.reductionTechnique = 'girard'; 
options.enclosureEnables = [1]; 
options.isHyperplaneMap = 0; 
% options.guardIntersect = 'polytope';
options.guardIntersect = 'conZonotope';

% Options for hybrid automata
options.x0 = [0;0;0;0] ; 
options.R0 = zonotope([options.x0,diag([0.01,0.01,0.1,0.1])]);  
options.timeStep = 1e-04 ; 
options.startLoc = {11, 31}; 
options.finalLoc = {0, 0}; 

% System Inputs
options.inputCompMap = [0];
options.uCompLoc = [];
options.UCompLoc = [];
options.uCompLocTrans = [];
options.uGlobTrans = 0;
options.uGlob = 0;
options.UGlob = zonotope(0);




% Simulation --------------------------------------------------------------

PHA = simulateRandom(PHA,10,0.5,0.5,20,options);
%PHA = simulate(PHA,options);



% Reachability Analysis ---------------------------------------------------

PHA = reach(PHA,options);




% Visualization -----------------------------------------------------------

% plot filter 1
figure 
box on
hold on
options.projectedDimensions = [1 2];
options.plotType = 'r';
plot(PHA,'reachableSet',options);
options.plotType = 'k';
plot(PHA,'simulation',options);
xlabel('$x_{1}$','interpreter','latex','FontSize',20);
ylabel('$x_{2}$','interpreter','latex','FontSize',20);
title('Filter 1');

% plot filter 2
figure 
box on
options.projectedDimensions = [3 4];
options.plotType = 'g';
plot(PHA,'reachableSet',options);
options.plotType = 'k';
plot(PHA,'simulation',options);
xlabel('$x_{1}$','interpreter','latex','FontSize',20);
ylabel('$x_{2}$','interpreter','latex','FontSize',20);
title('Filter 2');



% Visited Locations -------------------------------------------------------

visitedLocations = get(PHA,'locationReach');

res = 1;

%------------- END OF CODE --------------