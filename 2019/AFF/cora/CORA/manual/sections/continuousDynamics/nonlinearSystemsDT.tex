Unlike all other systems, the class \texttt{nonlinearSysDT} considers discrete time models. In more detail, the class represents nonlinear discrete time systems, which are defined by the following equation: 
\begin{equation*}
 x(k+1) = f \left( x(k),u(k) \right), ~~~~ x(0) \in \mathcal{X}_0 \subset \mathbb{R}^n, ~~ \forall k: u(k) \in \mathcal{U} \subset \mathbb{R}^m
\end{equation*}
The class provides functionality for simulation as well as for reachable set computation. 

\subsubsection{Method \texttt{reach}}  

This method computes the reachable set for the specified time horizon. Since the system evolves in discrete time, the task of calculating the reachable set is identical to the computation of the image of the nonlinear function $f(\cdot)$, where the sets $\mathcal{X}_k$ and $\mathcal{U}$ are the function inputs. Similar to continuous-time nonlinear systems, CORA applies the conservative linearization technique to calculate the image of the function. More specifically, we first generate the Taylor series of the nonlinear function $f \left( x(k),u(k) \right)$ up to a certain order $p$, using the center of the sets $\mathcal{X}_k$ and $\mathcal{U}$ as the combined expansion point. For all summands of the Taylor series we compute the set of possible values for $x(k) \in \mathcal{X}_k$ and $u(k) \in \mathcal{U}_k$. The set-valued summands are then added using Minkowski addition, which results in an approximation of the image of the function. Finally, in order to guarantee that the calculated reachable set is an over-approximation, we have to consider the Lagrange remainder of order $p+1$, which we evaluate with interval arithmetics. There exist some important settings that significantly influence the tightness of the calculated reachable set:
\begin{itemize}
 \item \texttt{options.taylorOrder:} Number of considered Taylor series terms $p$. An increase in the number of terms usually leads to a tighter reachable set. The maximum value that is supported by CORA is \texttt{options.taylorOrder = 3}.
 \item \texttt{options.errorOrder:} Zonotope order to which the set $\mathcal{X}_k$ is reduced before the quadratic or higher order terms of the Taylor series are evaluated. The reduction is necessary since the evaluation of quadratic or higher order terms for a zonotope results in a large increase in the number of zonotope generators. 
\end{itemize}  
Computing the Minkowski sum as well as the evaluation of quadratic or higher order Taylor series terms leads to zonotopes with a quickly increasing number of generators. In order to keep the runtime of the reachability algorithm reasonable, we therefore reduce the zonotope order of the set $\mathcal{X}_k$ after each time step to the user-defined value specified in \texttt{options.zonotopeOrder}.

\paragraph{Note:} 
\begin{sloppypar}
We have not implemented a dedicated class for computing reachable sets of linear discrete time systems. Given the linear discrete-time system $x(k+1)=Ax(k)+u(k)$, $\forall k: u(k) \in \mathcal{U}$, one can simply obtain the reachable sets using the following code: $\mathtt{R\{i+1\} = A*R\{i\} + U}$. A full example can be found under \texttt{CORA/examples/contDynamics/linear/example\_linear\_reach\_05\_discreteTime.m}.
\end{sloppypar}
