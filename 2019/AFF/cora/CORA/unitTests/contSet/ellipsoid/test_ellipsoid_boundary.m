function res = test_ellipsoid_boundary
% test_ellipsoid_boundary - unit test function of boundary
%
% Syntax:  
%    res = test_ellipsoid_boundary
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Matthias Althoff
% Written:      26-July-2016
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
passed=true;
E = ellipsoid.generate();
B = boundary(E,1000);
[V,D] = eig(E.Q);
[~,ind] = sort(diag(D),'descend');
D = D(ind,ind);
V = V(:,ind);
if ~E.isdegenerate
    for i=1:length(B)
        b = B(:,i);
        if (1-(b-E.q)'*inv(E.Q)*(b-E.q))>E.TOL
            passed = false;
            break;
        end
    end
else
    Vp = V(:,1:E.dim);
    Vn = V(:,E.dim+1:end);
    Qp = D(1:E.dim,1:E.dim);
    for i=1:length(B)
        b = B(:,i);
        b0 = b-E.q;
        if ~all(abs(Vn'*b0)<=E.TOL)
            passed = false;
            break;
        end
        bp = Vp'*b0;
        if abs(1-bp'*inv(Qp)*bp)>E.TOL
            passed = false;
            break;
        end
    end
end
if passed
    disp('test_ellipsoid_mtimes successful');
else
    disp('test_ellipsoid_mtimes failed');
end

%------------- END OF CODE --------------
