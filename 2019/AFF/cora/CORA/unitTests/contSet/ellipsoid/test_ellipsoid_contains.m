function res = test_ellipsoid_mtimes
% test_ellipsoid_mtimes - unit test function of mtimes
%
% Syntax:  
%    res = test_ellipsoid_mtimes
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Matthias Althoff
% Written:      26-July-2016
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
%NOTICE: Before executing this test, make sure that test_ellipsoid_boundary
%works since this test uses boundary.
N = 1000;
E = ellipsoid.generate();
samples = sample(E,N);
if ~all(contains(E,samples))
    disp('test_ellipsoid_mtimes failed');
else
    disp('test_ellipsoid_mtimes successful');
end

%------------- END OF CODE --------------