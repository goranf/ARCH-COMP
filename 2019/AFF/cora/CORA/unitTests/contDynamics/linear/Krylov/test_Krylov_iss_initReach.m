function res = test_Krylov_iss_initReach(~)
% test_Krylov_iss_initReach - unit_test_function for checking
% the Krylov method for the solution of the first time interval using the
% ISS model
%
% Syntax:  
%    res = test_Krylov_iss_initReach(~)
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      13-November-2018
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------

% load system matrices
load('iss.mat');
dim = length(A);

%set options --------------------------------------------------------------
R0 = interval(-0.0001*ones(dim,1),0.0001*ones(dim,1));
options.x0=mid(R0); %initial state for simulation

options.taylorTerms=6; %number of taylor terms for reachable sets
options.zonotopeOrder=30; %zonotope order
options.saveOrder = 1;
options.originContained=0;
options.reductionTechnique='girard';
options.linAlg = 2;
options.compOutputSet = 0;
options.outputOrder = 10;

U = interval([0;0.8;0.9],[0.1;1;1]);
options.uTrans=mid(U); %center of input set
options.U=zonotope([zeros(3,1),diag(rad(U))]); %input for reachability analysis

options.R0=zonotope(R0); %initial state for reachability analysis
options.tStart=0; %start time
options.tFinal=20; %final time
options.timeStep = 0.01;

options.KrylovError = eps;
options.KrylovStep = 20;
%--------------------------------------------------------------------------

%obtain factors for initial state and input solution
for i=1:(options.taylorTerms+1)
    %time step
    r = options.timeStep;
    %compute initial state factor
    options.factor(i)= r^(i)/factorial(i);    
end

%specify continuous dynamics-----------------------------------------------
linDyn = linearSys('iss',A,B,[],1);
%--------------------------------------------------------------------------

% copy for Krylov techniques
linDyn_Krylov = linDyn;
options_Krylov = options;

% compute overapproximation using Krylov methods
[Rnext, options] = initReach_Euclidean(linDyn, options.R0, options);

% compute overapproximation using Krylov methods
[Rnext, options_Krylov] = initReach_Krylov(linDyn_Krylov, options_Krylov.R0, options_Krylov);

% check whether solutions of Krylov method enclose those of the standard 
% method; to save Computational time, the results of the Krylov method are
% boxed
% homogeneous solution; time point
expFactor = 1+1e-6;
Rhom_tp_Krylov_boxed = interval(options_Krylov.Rhom_tp_proj);
Rhom_tp_Krylov_boxed = enlarge(Rhom_tp_Krylov_boxed,expFactor);
Rhom_tp_boxed = interval(options.Rhom_tp);
res(1) = Rhom_tp_boxed <= Rhom_tp_Krylov_boxed;

% homogeneous solution; time interval
Rhom_Krylov_boxed = interval(options_Krylov.Rhom_proj);
Rhom_Krylov_boxed = enlarge(Rhom_Krylov_boxed,expFactor);
Rhom_boxed = interval(options.Rhom);
res(2) = Rhom_boxed <= Rhom_Krylov_boxed;

% particulate solution
Raux_Krylov_boxed = interval(options_Krylov.Raux_proj);
Raux_Krylov_boxed = enlarge(Raux_Krylov_boxed,expFactor);
Raux_boxed = interval(options.Raux);
res(3) = Raux_boxed <= Raux_Krylov_boxed;

% particulate solution of uTrans
Rtrans_Krylov_boxed = interval(options_Krylov.Rtrans_proj);
Rtrans_Krylov_boxed = enlarge(Rtrans_Krylov_boxed,expFactor);
Rtrans_boxed = interval(options.Rtrans);
res(4) = Rtrans_boxed <= Rtrans_Krylov_boxed;

% Is exact solution in zonotope?
res = in(R_exact, Rnext.tp);

%------------- END OF CODE --------------