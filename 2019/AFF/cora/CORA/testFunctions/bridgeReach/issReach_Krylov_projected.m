function issReach_Krylov_projected()
% updated: 20-October-2018, MA

%start parallel computing
try
    parpool
catch
    disp('parallel sessions already running')
end

%system matrix
load('iss.mat');

% % obtain A and B marices
% A = G.A;
% B = G.B;

%obtain dimension
dim = length(A);

%set options --------------------------------------------------------------
%R0 = interval(-0.0001*ones(dim,1),0.0001*ones(dim,1));


options.tStart = 0; %start time
%options.tFinal = 1; %final time
options.tFinal = 20; %final time
options.x0=zeros(dim,1); %initial state for simulation
I = eye(dim);
options.R0=zonotope(sparse([options.x0,0.001*I(:,1:10)])); %initial state for reachability analysis

options.timeStep = 0.01; %time step size for reachable set computation
options.taylorTerms = 6; 
options.zonotopeOrder = 30; %zonotope order
options.saveOrder = 1;
options.reductionTechnique='girard'; 
%options.KrylovError = 1e-6;
options.KrylovError = eps;
options.KrylovStep = 20;

options.plotType='frame';
%options.originContained = 0;
options.originContained = 1;
%--------------------------------------------------------------------------


%obtain uncertain inputs
%U = interval([0;0.8;0.9],[0.1;1;1]);
%U = interval([-0.1;-1;-1],[0.1;1;1]);
U = interval([0;0;0]);
options.uTrans=mid(U); %center of input set
options.U=zonotope([zeros(3,1),diag(rad(U))]); %input for reachability analysis

%specify continuous dynamics-----------------------------------------------
issDyn=linearSys('issDynamics',A,B,[],C); %initialize quadratic dynamics
%--------------------------------------------------------------------------


%compute reachable set using zonotope bundles
profile on
tic
Rcont = reach(issDyn, options);
toc 
profile off
profile viewer

%simulate
stepsizeOptions = odeset('MaxStep',0.2*(options.tFinal-options.tStart));
%generate overall options
opt = odeset(stepsizeOptions);

%initialize
runs=40;
finalTime=options.tFinal;

for i=1:runs

    %set initial state, input
    if i<=30
        options.x0=randPointExtreme(options.R0); %initial state for simulation
    else
        options.x0=randPoint(options.R0); %initial state for simulation
    end

    %set input
    if i<=8
        options.u=randPointExtreme(options.U)+options.uTrans; %input for simulation
    else
        options.u=randPoint(options.U)+options.uTrans; %input for simulation
    end

    %simulate hybrid automaton
    [issDyn,t{i},x{i}] = simulate(issDyn,options,options.tStart,options.tFinal,options.x0,opt); 
    x_proj{i} = (issDyn.C*x{i}')';
end


%plot
for plotRun=1:2
    if plotRun==1
        projectedDimensions=[1 2];
    elseif plotRun==2
        projectedDimensions=[2 3];
    end

    figure;
    hold on

    %plot reachable sets of zonotope
    for i=1:length(Rcont)
        Zproj = project(Rcont{i},projectedDimensions);
        Zproj = reduce(Zproj,'girard',100);
        plotFilled(Zproj,[1,2],[.8 .8 .8],'EdgeColor','none');
    end

    %plot initial set
    R0_proj = issDyn.C*options.R0;
    plot(R0_proj, projectedDimensions, 'w-', 'lineWidth', 2);

    %plot simulation results      
    for i=1:length(t)
            plot(x_proj{i}(:,projectedDimensions(1)),x_proj{i}(:,projectedDimensions(2)),'Color',0*[1 1 1]);
    end

    xlabel(['x_{',num2str(projectedDimensions(1)),'}']);
    ylabel(['x_{',num2str(projectedDimensions(2)),'}']);
end




function Rcont = contReach(sys, options)

%obtain factors for initial state and input solution
for i=1:(options.taylorTerms+1)
    %time step
    r = options.timeStep;
    %compute initial state factor
    options.factor(i)= r^(i)/factorial(i);    
end

%initialize reachable set computations
[Rnext, options] = initReach_Krylov_projected(sys, options.R0, options);

% if precomputation is used
if sys.krylov.precomputed == 1
    Rinit = Rnext;
end

%while final time is not reached
t=options.tStart;
iSet=1;

while t<options.tFinal
    
    %save reachable set in cell structure
    Rcont{iSet}=Rnext.ti; 
    
    %increment time and set counter
    t = t+options.timeStep;
    iSet = iSet+1; 
    options.t=t;
    %t
    
    %profile on
    %compute next reachable set
    if sys.krylov.precomputed == 1
        [Rnext,options] = post_Krylov_projected(sys,Rinit,options); 
    else
        disp('warning: projection cannot be used')
    end
    %profile viewer
end


        
