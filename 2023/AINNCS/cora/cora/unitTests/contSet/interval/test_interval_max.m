function res = test_interval_max
% test_interval_max - unit test function of dim
%
% Syntax:  
%    res = test_interval_max
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Tobias Ladner
% Written:      16-December-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

res = true;

% basic examples
I1 = interval([-2;-1],[2;1]);
Im = max(I1, 0);
res = res && isequal(Im, interval([0;0], [2;1]));

I2 = interval([-1;1], [1;3]);
Im = max(I1, I2);
res = res && isequal(Im, interval([-1;1], [2;3]));

% other set representations
Z = zonotope([1;-1], [2;1]);
Im = max(I1, Z);
res = res && isequal(Im, interval([-1;-1], [3;1]));

% empty case
I = interval();
Im = max(I, I);
res = res & isempty(Im);

I = interval();
Im = max(I, []);
res = res & isempty(Im);



%------------- END OF CODE --------------
