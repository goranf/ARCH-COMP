function [res] = test_nn_neuralNetwork_verify()
% test_nn_neuralNetwork_verify - tests the verify function 
%    
%
% Syntax:  
%    res = test_nn_neuralNetwork_verify()
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Tobias Ladner
% Written:      01-December-2022
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

res = example_neuralNetwork_verify_01_unsafe_verified();
res = res & example_neuralNetwork_verify_02_unsafe_falsified();
res = res & example_neuralNetwork_verify_03_safe_verified();
res = res & example_neuralNetwork_verify_04_safe_falsified();

end

%------------- END OF CODE --------------



