function res = test_CHECKS_ENABLED()
% test_CHECKS_ENABLED - whether checks are enabled
%
% Syntax:  
%    res = test_CHECKS_ENABLED()
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: batchCombinator

% Author:       Tobias Ladner
% Written:      17-February-2023
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

res = CHECKS_ENABLED;

%------------- END OF CODE --------------