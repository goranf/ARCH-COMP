function nn = addVisualizationLayers(obj, neuronIds, verbose)
% addVisualizationLayers - insert visualization layers
%
% Syntax:
%    nn = addVisualizationLayers(obj, verbose)
%
% Inputs:
%    neuronIds - array, ids of neurons to visualize
%    verbose - bool if information should be displayed
%
% Outputs:
%    nn - new neural network with visualization layers inserted
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: NeuralNetwork

% Author:       Lukas Koller, Tobias Ladner
% Written:      23-June-2022
% Last update:  ---
% Last revision:17-August-2022

%------------- BEGIN CODE --------------
n = length(obj.layers);
layers = cell(2*n+1, 1);
if nargin < 2
    % always visualize neuron 1 & 2 per layer
    ids = ones(n+1, 1);
    neuronIds = [ids, 2 * ids];
end
if nargin < 3
    verbose = false;
end

lastLayerName = "Input";
layers{1} = nnVisualizationLayer(1, neuronIds(1, :), strcat("VisualizationLayer(1): ", lastLayerName));
for i = 1:n
    layers{2*i} = obj.layers{i};
    visName = strcat("VisualizationLayer(", num2str(i+1), "): ", lastLayerName, ' -> ', obj.layers{i}.name);
    layers{2*i+1} = nnVisualizationLayer(i+1, neuronIds(i+1, :), visName);
    lastLayerName = obj.layers{i}.name;
end
layers = reshape(layers, [], 1); % 1 column

if verbose
    disp("The network has the following layers:")
    disp(layers)
    fprintf("(%d layers)\n", size(layers, 1))
end

nn = neuralNetwork(layers);
end

%------------- END OF CODE --------------