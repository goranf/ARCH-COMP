classdef nnGNNLinearLayer < nnGNNLayer
% nnGNNLinearLayer - class for linear layers
%
% Syntax:
%    obj = nnGNNLinearLayer(W, b, name)
%
% Inputs:
%    W - weight matrix
%    b - bias column vector
%    name - name of the layer, defaults to type
%
% Outputs:
%    obj - generated object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: neuralNetwork

% Author:       Tianze Huang, Gerild Pjetri, Tobias Ladner
% Written:      22-December-2022
% Last update:  23-February-2023 (TL: clean up)
% Last revision:---

%------------- BEGIN CODE --------------

properties (Constant)
    is_refinable = false
end

properties
    W, b
end

methods
    % constructor
    function obj = nnGNNLinearLayer(W, varargin)
        % parse input
        [b, name] = setDefaultValues({0, []}, varargin);
        inputArgsCheck({ ...
            {W, 'att', 'numeric'}; ...
            {b, 'att', 'numeric'}; ...
        })

        % check dimensions
        if length(b) == 1
            b = b * ones(size(W, 1), 1);
        end
        if ~all(size(b, 1) == size(W, 1))
           throw(CORAerror('CORA:wrongInputInConstructor', ...
               'The dimensions of W and b should match.'));
        end
        if size(b, 2) ~= 1
           throw(CORAerror('CORA:wrongInputInConstructor', ...
               "Second input 'b' should be a column vector."));
        end

        % call super class constructor
        obj@nnGNNLayer(name)

        obj.W = double(W);
        obj.b = double(b);
    end

    function outputSize = getOutputSize(obj, inputSize, graph)
        if nargin < 3
            nrNodes = 1;
        else
            nrNodes = obj.computeNrNodes(graph);
        end  
        nrOutFeatures = size(obj.W,1);
        outputSize = [nrNodes*nrOutFeatures, 1];
    end

    function [nin, nout] = getNumNeurons(obj)
        nin = size(obj.W, 2);
        nout = size(obj.W, 1);
    end
end

% evaluate ----------------------------------------------------------------

methods  (Access = {?nnLayer, ?neuralNetwork})
    
    % numeric
    function r = evaluateNumeric(obj, input, evParams)
        [Wv, bv] = computeVecLinear(obj, evParams.graph);
        r = Wv * input + bv;
        r = full(r);
    end

    % sensitivity
    function S = evaluateSensitivity(obj, S, x, evParams)
        [Wv, ~] = computeVecLinear(obj, evParams.graph);
        S = S * Wv;
    end

    % zonotope/polyZonotope
    function [c, G, Grest, expMat, id, id_, ind, ind_] = evaluatePolyZonotope(obj, c, G, Grest, expMat, id, id_, ind, ind_, evParams)
        [Wv, bv] = computeVecLinear(obj, evParams.graph);
        c = Wv * c + bv;
        G = Wv * G;
        Grest = Wv * Grest;
        
        % make it full again
        c = full(c);
        G = full(G);
        Grest = full(Grest);
    end
end

% Auxiliary functions -----------------------------------------------------

methods(Access=protected)
    function [Wv, bv] = computeVecLinear(obj, graph)
        % computes vectorized weight matrix
        nrNodes = computeNrNodes(obj, graph);
        Wv = kron(speye(nrNodes), obj.W);
        bv = kron(ones(nrNodes,1),obj.b);
    end
end

end

%------------- END OF CODE --------------