# ARCH-COMP 2023: AINNCS Category

## NNV
MATLAB Toolbox for Neural Network Verification

This toolbox implements reachability methods for analyzing neural networks, particularly with a focus on closed-loop controllers in autonomous cyber-physical systems (CPS).

## Related tools and software

This toolbox makes use of the neural network model transformation tool ([nnmt](https://github.com/verivital/nnmt)) and for closed-loop systems analysis, the hybrid systems model transformation and translation tool ([HyST](https://github.com/verivital/hyst)), and the COntinuous Reachability Analyzer ([CORA](https://github.com/TUMcps/CORA)).

# Execution without local installation:

This folder contains the code aand a Dockerfile to run all the benchmarks with one command.

However, you need to provide a MATLAB Licence file `license.lic` to run the code:
- Create a MATLAB License file: 
	For the docker container to run MATLAB, one has to create a new license file for the container.
	Log in with your MATLAB account at https://www.mathworks.com/licensecenter/licenses/
	Click on your license, and then navigate to
	1. "Install and Activate"
	1. "Activate to Retrieve License File"
	1. "Activate a Computer"
	(...may differ depending on how your licensing is set up).
- Choose:
	- Release: `R2022b`
	- Operating System: `Linux`
	- Host ID: `12:34:56:78:9a:bc` (= Default MAC-Adress assigned when running docker in `submit.sh`) | your host MAC Adress
	- Computer Login Name: `root`
	- Activation Label: `<any name>`
- Download the file and place it next to the docker file

- If one is using a different MAC-Address than `12:34:56:78:9a:bc`, one needs to change the argument `mac-address` in the `submit.sh` script to the MAC-Address selected when creating the MATLAB license file.

If you are having trouble, don't hesitate contacting us: <a href="diego.manzanas.lopez@vanderbilt.edu">diego.manzanas.lopez@vanderbilt.edu</a>

## Run the code

You can run all benchmarks using a docker container by running the `submit.sh` script.

	./submit.sh
	
The results will be stored to `./results`.


# Execution locally
	
Alternatively, open this directory in MATLAB and run `submit.m`, but **ONLY** after all packages and toolboxes required by NNV are intalled. The installation process for NNV is detailed below:

## Installation:
    1) Install Matlab (2022b or newer) with at least the following toolboxes:
       Computer Vision
       Control Systems
       Deep Learning
       Image Processing
       Optimization
       Parallel Computing
       Statistics and Machine Learning
       Symbolic Math
       System Identification
   
    1.a) Install the following support package
       Deep Learning Toolbox Converter for ONNX Model Format (https://www.mathworks.com/matlabcentral/fileexchange/67296-deep-learning-toolbox-converter-for-onnx-model-format)
       
       Note: Support packages can be installed in MATLAB's HOME tab > Add-Ons > Get Add-ons, search for the support package using the Add-on Explorer and click on the Install button.

    2) Open matlab, then go to the directory where nnv exists on your machine, then run the `install.m` script located at `nnv/code/nnv/`
    
Note: if you restart Matlab, rerun either `install.m` or `startup_nnv.m`, which will add the necessary dependencies to the path; you alternatively can run savepath after installation to avoid this step after restarting Matlab, but this may require administrative privileges


