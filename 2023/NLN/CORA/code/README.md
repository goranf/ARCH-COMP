## Results for CORA

The MATLAB script [run_arch23_nln_category.m](/code/run_arch23_nln_category.m) reproduces the results for the reachability toolbox CORA. In particular, the script computes the occupancy sets for all traffic scenarios and saves them in the directory **/results/solutions**. 

## Collision Checker

The python file [check_collision.py](/code/check_collision.py) runs the CommonRoad drivability checker to check if the exported occupancy sets collide with static or dynamic obstacles from the traffic scenarios. Details on how to install the CommonRoad drivability checker can be found in the file [postInstall](/environment/postInstall).