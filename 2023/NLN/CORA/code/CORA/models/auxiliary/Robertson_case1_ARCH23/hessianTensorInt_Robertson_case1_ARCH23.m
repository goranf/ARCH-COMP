function Hf=hessianTensorInt_Robertson_case1_ARCH23(x,u)



 Hf{1} = interval(sparse(4,4),sparse(4,4));

Hf{1}(3,2) = 100;
Hf{1}(2,3) = 100;


 Hf{2} = interval(sparse(4,4),sparse(4,4));

Hf{2}(2,2) = -2000;
Hf{2}(3,2) = -100;
Hf{2}(2,3) = -100;


 Hf{3} = interval(sparse(4,4),sparse(4,4));

Hf{3}(2,2) = 2000;

end