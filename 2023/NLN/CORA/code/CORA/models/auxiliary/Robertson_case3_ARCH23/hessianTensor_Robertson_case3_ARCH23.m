function Hf=hessianTensor_Robertson_case3_ARCH23(x,u)



 Hf{1} = sparse(4,4);

Hf{1}(3,2) = 1000;
Hf{1}(2,3) = 1000;


 Hf{2} = sparse(4,4);

Hf{2}(2,2) = -20000000;
Hf{2}(3,2) = -1000;
Hf{2}(2,3) = -1000;


 Hf{3} = sparse(4,4);

Hf{3}(2,2) = 20000000;

end