function [A,B]=jacobian_coupledVanDerPol_ARCH23(x,u)

A=[0,1,0,0,0;...
- x(5) - 2*x(1)*x(2) - 1,1 - x(1)^2,x(5),0,x(3) - x(1);...
0,0,0,1,0;...
x(5),0,- x(5) - 2*x(3)*x(4) - 1,1 - x(3)^2,x(1) - x(3);...
0,0,0,0,0];

B=[0;...
0;...
0;...
0;...
0];

