FROM wolframresearch/wolframengine:13.0.1 AS wolfram

USER root
ARG DEBIAN_FRONTEND=noninteractive
WORKDIR /root

# Copy Wolfram Engine licence key (Wolfram engine is already installed in base image)
COPY mathpass /root/.WolframEngine/Licensing/mathpass
RUN chmod 777 /root/.WolframEngine/Licensing/mathpass

# Install python 3.9
RUN apt-get update -y
RUN apt-get install -y software-properties-common
RUN add-apt-repository -y ppa:deadsnakes/ppa
RUN apt-get update -y && apt-get upgrade -y
RUN apt install -y python3.9 python3.9-dev python3.9-venv
RUN python3.9 -m ensurepip

# Install required python packages
RUN python3.9 -m pip install lark
RUN python3.9 -m pip install sympy
RUN python3.9 -m pip install scipy
RUN python3.9 -m pip install wolframclient
RUN python3.9 -m pip install gevent-websocket
RUN python3.9 -m pip install z3-solver==4.8.14.0
RUN python3.9 -m pip install flask


# Install git
RUN apt install -y git

# Copy the HHLPy project repo
COPY mars ./mars
RUN chmod -R 777 ./mars

WORKDIR /root/mars

COPY examples ./examples
RUN chmod -R 777 ./examples
COPY results ./results
RUN chmod -R 777 ./results
COPY runEvaluations.py ./runEvaluations.py
RUN chmod 777 ./runEvaluations.py

WORKDIR /root/mars/hhlpy/gui

ENV PYTHONPATH "${PYTHONPATH}:/root/mars"

# replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# nvm environment variables
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 16.18.0

# update the repository sources list
# and install dependencies
RUN apt-get update \
    && apt-get install -y curl \
    && apt-get -y autoclean

RUN apt-get install -y sudo
RUN curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
RUN sudo apt install -y nodejs


# add node and npm to path so the commands are available
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH
RUN npm install

EXPOSE 8000
EXPOSE 8080

CMD ["npx", "concurrently", "-c", "green,blue", "-n", "pythonserver,vueserver", "python3.9 ./server.py", "vue-cli-service serve"]