from os import listdir
from os.path import isfile, join, dirname, abspath
import time
import re
import csv

from hhlpy.wolframengine_wrapper import session
from ss2hcsp.hcsp.parser import parse_hoare_triple_with_meta
from hhlpy.hhlpy import CmdVerifier

def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower() 
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)] 
    return sorted(l, key=alphanum_key)

def eval(file):

    print("Running {}. ".format(file), end ="")

    tic = time.perf_counter()
    file = join(path, file)
    file = open(file, mode='r', encoding='utf-8')
    file_contents = file.read()
    file.close()

    # Parse pre-condition, HCSP program, and post-condition
    hoare_triple = parse_hoare_triple_with_meta(file_contents)

    # Initialize the verifier
    verifier = CmdVerifier(
                pre=hoare_triple.pre, 
                hp=hoare_triple.hp,
                post=hoare_triple.post,
                constants=hoare_triple.constants,
                functions=hoare_triple.functions)

    # Compute wp and verify
    verifier.compute_wp()
    res = verifier.verify()
    toc = time.perf_counter()
    if res:
        result = "verified"
        print(f"Completed in {toc - tic:0.4f} seconds.")
    else:
        result = "failed"
        print(f"Failed after {toc - tic:0.4f} seconds.")

    return result, toc - tic


if __name__ == "__main__":
    print("Starting Wolfram Engine.")

    with session:
        example_categs = ["Basic", "Nonlinear", "Simulink"]
        with open(
            join(dirname(abspath(__file__)), "results", "results.csv"),
            mode="w",
            newline="",
        ) as handler:
            fieldnames = ["benchmark", "instance", "result", "time"]
            writer = csv.DictWriter(handler, fieldnames=fieldnames)

            writer.writeheader()
            for title in example_categs:
                
                print(f"=== {title} ===")
                
                path = join(dirname(__file__), "examples", title)
                file_names = natural_sort([f for f in listdir(path) if isfile(join(path, f))])

                for file in file_names:
                    result, t = eval(file)

                    writer.writerow(
                        {"benchmark": title, "instance": file, "result": result, "time": t}
                    )
