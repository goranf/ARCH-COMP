function Hf=hessianTensor_roessler(x,u)



 Hf{1} = sparse(4,4);



 Hf{2} = sparse(4,4);



 Hf{3} = sparse(4,4);

Hf{3}(3,1) = 1;
Hf{3}(1,3) = 1;
